
package com.cashback.cashbackarab.application;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cashback.cashbackarab.healthcheck.CashBackArabHealthCheck;
import com.cashback.cashbackarab.provider.filter.AuthorizationFilter;
import com.cashback.cashbackarab.provider.mapper.CustomExceptionMapper;
import com.cashback.cashbackarab.resources.impl.BrandResourceImpl;
import com.cashback.cashbackarab.resources.impl.ContentResourceImpl;
import com.cashback.cashbackarab.resources.impl.NotificationResourceImpl;
import com.cashback.cashbackarab.resources.impl.TransactionResourceImpl;
import com.cashback.cashbackarab.resources.impl.TrendingDealsResourceImpl;
import com.cashback.cashbackarab.resources.impl.UserResourceImpl;
import com.cashback.cashbackarab.resources.impl.UserResourceImplV2;
import com.google.common.collect.Lists;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import io.dropwizard.Application;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;

public class CashBackApplication extends Application<CashBackApplicationConfiguration> implements Managed {

	private ClassPathXmlApplicationContext classPathXmlApplicationContext;

	public static void main(String[] args) throws Exception {
		new CashBackApplication().run(Lists.newArrayList("server", "cashbackarab.yaml").toArray(new String[2]));
	}

	@Override
	public void start() throws Exception {

	}

	@Override
	public void run(CashBackApplicationConfiguration configuration, Environment environment) throws Exception {
		classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring/application-config.xml");
		final CashBackArabHealthCheck healthCheck = classPathXmlApplicationContext
				.getBean(CashBackArabHealthCheck.class);
		environment.healthChecks().register("Cashback  Health Check", healthCheck);
		final AuthorizationFilter authorizationFilter = classPathXmlApplicationContext
				.getBean(AuthorizationFilter.class);
		final BrandResourceImpl brandResourceImpl = classPathXmlApplicationContext.getBean(BrandResourceImpl.class);
		final TrendingDealsResourceImpl trendingDealsResourceImpl = classPathXmlApplicationContext
				.getBean(TrendingDealsResourceImpl.class);
		final UserResourceImpl userResourceImpl = classPathXmlApplicationContext.getBean(UserResourceImpl.class);
		final CustomExceptionMapper cem = classPathXmlApplicationContext.getBean(CustomExceptionMapper.class);
		final ContentResourceImpl contentResourceImpl = classPathXmlApplicationContext
				.getBean(ContentResourceImpl.class);
		final NotificationResourceImpl notificationResourceImpl = classPathXmlApplicationContext
				.getBean(NotificationResourceImpl.class);
		final TransactionResourceImpl transactionResourceImpl = classPathXmlApplicationContext
				.getBean(TransactionResourceImpl.class);
		final UserResourceImplV2 userResourceImplV2 = classPathXmlApplicationContext
				.getBean(UserResourceImplV2.class);

		
		
		environment.jersey().packages("com.cashback.cashbackarab.provider");
		environment.jersey().register(contentResourceImpl);
		environment.jersey().register(notificationResourceImpl);
		environment.jersey().register(transactionResourceImpl);
		environment.jersey().register(authorizationFilter);
		environment.jersey().register(brandResourceImpl);
		environment.jersey().register(trendingDealsResourceImpl);
		environment.jersey().register(userResourceImpl);
		environment.jersey().register(userResourceImplV2);
		environment.jersey().register(cem);
		environment.lifecycle().manage(this);
	}

	@Override
	public void stop() throws Exception {
		final ComboPooledDataSource comboPooledDataSource = classPathXmlApplicationContext
				.getBean(ComboPooledDataSource.class);
		comboPooledDataSource.close();
		classPathXmlApplicationContext.close();
	}
}
