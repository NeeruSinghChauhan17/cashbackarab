package com.cashback.cashbackarab.mail;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.cashback.cashbackarab.dao.MailDao;
import com.cashback.cashbackarab.models.Email;

@Service
public class EmailServiceImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

	private static final String UTF_8_ENCODING = "UTF-8";

	private final JavaMailSender mailSender;

	private final VelocityEngine velocityEngine;

	private final String fromContactId;

	private final String adminMailId;
	
	private MailDao mailDao;

	@Autowired
	public EmailServiceImpl(final JavaMailSender mailSender, final VelocityEngine velocityEngine,
			@Value("${email.from.contactId}") final String fromContactId,
			@Value("${email.from.adminMailId}") final String adminMailId, final MailDao mailDao) {
		this.mailSender = mailSender;
		this.velocityEngine = velocityEngine;
		this.fromContactId = fromContactId;
		this.adminMailId = adminMailId;
		this.mailDao=mailDao;
	}

	public void sendWelcomeMail(final String email, String url, String fname) {
		try {
			Email emailContent=mailDao.getEmailContent("activate2");
			final MimeMessage message = mailSender.createMimeMessage();
			final MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setSubject(emailContent.getEmailSubject());
			helper.setFrom(fromContactId);
			helper.setTo(email);
			String welcomeMessage=emailContent.getEmailMessage();
			final Map<String, Object> model = new HashMap<String, Object>();
			//model.put("url", url);
			//model.put("welcomeText", "Hi There!!");
			welcomeMessage=welcomeMessage.replace("{activate_link}"," "+ url);
			welcomeMessage=welcomeMessage.replace("{first_name}", " "+fname);
			model.put("welcomeMessage", welcomeMessage);
			@SuppressWarnings("deprecation")
			final String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "UserVerificationMail.vm",
				 model);
			message.setContent(text, "text/html;charset=utf-8");
			mailSender.send(message);
		} catch (final MessagingException e) {
			LOGGER.error("Error Sending Email to {}", e);
		}

	}

	public void sendForgotPasswordMail(final String email, final String password,String fname) {
		try {
			String loginUrl="https://cashbackarab.com/login.php";
			Email emailContent=mailDao.getEmailContent("forgot_password");
			final MimeMessage message = mailSender.createMimeMessage();
			final MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setSubject(emailContent.getEmailSubject());
			helper.setFrom(fromContactId);
			helper.setTo(email);
			String forgotPassword=emailContent.getEmailMessage();
			final Map<String, Object> model = new HashMap<String, Object>();
			/*model.put("fname", fname);
			model.put("email", email);
			model.put("password", password);*/
			forgotPassword =forgotPassword.replace("{first_name}"," "+ fname);
			forgotPassword =forgotPassword.replace("{username}",email);
			forgotPassword =forgotPassword.replace("{password}",password);
			forgotPassword=forgotPassword.replace("{login_url}", loginUrl);
			model.put("forgotPassword", forgotPassword);
			System.out.println(forgotPassword);
			@SuppressWarnings("deprecation")
			final String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "ForgotPasswordMail.vm",
					model);
			message.setContent(text, "text/html;charset=utf-8");

			mailSender.send(message);
		} catch (final MessagingException e) {
			LOGGER.error("Error Sending Email to {}", e, email);
		}

	}

	public void sendTransactionMail(final String userMailId, final Double amount) {
		try {
			final MimeMessage message = mailSender.createMimeMessage();
			final MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setSubject("Transaction Confirmation Mail");
			helper.setFrom(fromContactId);
			helper.setTo(adminMailId);
			helper.setCc(userMailId);
			final Map<String, Object> model = new HashMap<String, Object>();
			model.put("welcomeText", "Hi There!!");
			final String text = "Hi Admin, \n" + userMailId + " has sent a request to credit " + amount + "$";
			helper.setText(text);
			mailSender.send(message);
		} catch (final MessagingException e) {
			LOGGER.error("Error Sending Email to {}", e, userMailId);
		}

	}

	/*
	 * public void sendMail(final String mail, String url){ SendGrid sendgrid =
	 * new SendGrid(
	 * "SG.bx_99_L7R9y7ttSmZB3nKw.nDmK_ZN3pJElgM6MmSEwPt-bsbxLh_lnK6J7Kt911bQ");
	 * 
	 * SendGrid.Email email = new SendGrid.Email();
	 * email.addTo("harishanuragi@gmail.com"); email.
	 * email.setFrom("hchandra@craterzone.com");
	 * email.setSubject("Hello World");
	 * //email.setText("My first email with SendGrid Java!"); final Map<String,
	 * Object> model = new HashMap<String, Object>(); model.put("url", url);
	 * model.put("welcomeText", "Hi There!!"); final String text =
	 * VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
	 * "UserVerificationMail.vm", UTF_8_ENCODING, model); email.setHtml(text);
	 * 
	 * try { SendGrid.Response response = sendgrid.send(email);
	 * System.out.println(response.getMessage()); } catch (SendGridException e)
	 * { System.err.println(e); } }
	 */
	
	
}