package com.cashback.cashbackarab.content;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class CashbackApp {

	private final String appName;
	private final String bucketFilePath;
	private final String bucketUrl;

	public CashbackApp() {
		this(null, null, null);
	}

	public CashbackApp(final String appName, final String bucketFilePath, final String bucketName) {
		this.appName = appName;
		this.bucketFilePath = bucketFilePath;
		this.bucketUrl = bucketName;
	}

	@JsonProperty("app_name")
	public String getAppName() {
		return appName;
	}

	@JsonProperty("path")
	public String getBucketFilePath() {
		return bucketFilePath;
	}

	@JsonProperty("bucket_url")
	public String getBucketUrl() {
		return bucketUrl;
	}

}