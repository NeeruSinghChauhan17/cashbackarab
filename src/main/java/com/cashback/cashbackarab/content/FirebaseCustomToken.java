package com.cashback.cashbackarab.content;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class FirebaseCustomToken {

	private final String token;

	@SuppressWarnings("unused")
	private FirebaseCustomToken() {
		this(null);
	}
    
	public FirebaseCustomToken(final String token) {
		this.token = token;
	}

	@JsonProperty("token")
	public String getToken() {
		return token;
	}

}
