package com.cashback.cashbackarab.notification.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cashback.cashbackarab.models.notification.Push;
import com.cashback.cashbackarab.notification.PushNotifier;
import com.crater.pushclient.FirebasePush;
import com.crater.pushclient.NotificationPayload;
import com.crater.pushclient.PushService;

@Service
public class PushNotifierImpl implements PushNotifier {

	private final PushService fireBasePushSender;

	@Autowired
	public PushNotifierImpl(final PushService fireBasePushSender) {
		this.fireBasePushSender = fireBasePushSender;
	}
   
	@Override
	public void sendPush(final Push push) {

		if (push.getPayload().isCustomPayload()) {
			// custom push with data payload
			push.getDevices().parallelStream().forEach(device -> fireBasePushSender.sendPushNotification(
					push.getSenderId(),
					FirebasePush.withToAndCustomData(device.getDeviceToken(), push.getPayload().getCustomData())));
		} else {
			// Display push (Notification payload)
			push.getDevices().parallelStream()
					.forEach(device -> fireBasePushSender.sendPushNotification(push.getSenderId(),
							FirebasePush.withToAndNotificationPayload(device.getDeviceToken(), NotificationPayload
									.withTitleAndBody(push.getPayload().getTitle(), push.getPayload().getBody()))));
		}
	}
}
