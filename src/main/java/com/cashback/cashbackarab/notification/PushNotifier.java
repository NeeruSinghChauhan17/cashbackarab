package com.cashback.cashbackarab.notification;

import com.cashback.cashbackarab.models.notification.Push;

public interface PushNotifier {

	void sendPush(Push push);
}
