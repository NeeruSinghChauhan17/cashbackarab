package com.cashback.cashbackarab.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cashback.cashbackarab.models.InternationalNumber;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

public class LibPhoneNumber {
	private static final Logger LOGGER = LoggerFactory.getLogger(LibPhoneNumber.class);

	public static InternationalNumber  getInternationalNumber(String number) {
	  if(!number.contains("+")){              //if no do not contains + before country-code + number
	     number="+"+number;	  
	  }
		String[] arr = new String[2];
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		 try {
		   
		    PhoneNumber numberProto = phoneUtil.parse(number, "");     // if no contains + ,cc and number
		   int countryCode = numberProto.getCountryCode();
		    long no=numberProto.getNationalNumber();
		  
		  	arr[0]=String.valueOf(countryCode);
		    arr[1]=String.valueOf(no);
		    
		  LOGGER.info("   InternationalNumber:    cc="+arr[0] +"   number="+arr[1] );
		} catch (NumberParseException e) {
		    LOGGER.error("NumberParseException was thrown: " + e.toString());
		}
		return new InternationalNumber(arr[0],null,arr[1]);
      }
  }
