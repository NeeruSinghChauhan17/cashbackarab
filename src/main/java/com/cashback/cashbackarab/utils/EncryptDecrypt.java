package com.cashback.cashbackarab.utils;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;



public class EncryptDecrypt {
	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptDecrypt.class);
	private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private static Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    private static String key = "1234abcd";
   public final static String accountSeperator = ":";

 /*   public EncryptDecrypt() throws Exception {
        myEncryptionKey = "ThisIsSpartaThisIsSparta";
        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
    }*/

    private static String decrypt(String message) throws Exception {
        byte[] bytesrc = convertHexString(message);
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        DESKeySpec desKeySpec = new DESKeySpec(key.getBytes("UTF-8"));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
        IvParameterSpec iv = new IvParameterSpec(key.getBytes("UTF-8"));

        cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
        byte[] retByte = cipher.doFinal(bytesrc);
        return new String(retByte);
      }

      private static String encrypt(String message) throws Exception {
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        DESKeySpec desKeySpec = new DESKeySpec(key.getBytes("UTF-8"));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
        IvParameterSpec iv = new IvParameterSpec(key.getBytes("UTF-8"));
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
        return toHexString(cipher.doFinal(message.getBytes("UTF-8")));
      }
      private static byte[] convertHexString(String ss) {
    	    byte digest[] = new byte[ss.length() / 2];
    	    for (int i = 0; i < digest.length; i++) {
    	      String byteString = ss.substring(2 * i, 2 * i + 2);
    	      int byteValue = Integer.parseInt(byteString, 16);
    	      digest[i] = (byte) byteValue;
    	    }
    	    return digest;
    	  }

    	  private static String toHexString(byte b[]) {
    	    StringBuffer hexString = new StringBuffer();
    	    for (int i = 0; i < b.length; i++) {
    	      String plainText = Integer.toHexString(0xff & b[i]);
    	      if (plainText.length() < 2)
    	        plainText = "0" + plainText;
    	      hexString.append(plainText);
    	    }
    	    return hexString.toString();
    	  }


    
    public static String[] decodeAccount(String cookieValue) {
        try {
          String origi = EncryptDecrypt.decrypt(cookieValue);
          String[] parts = origi.split(EncryptDecrypt.accountSeperator);
          if (parts.length == 2 && !parts[0].equals("") && !parts[1].equals("")) {
            return parts;
          }
        } catch (Exception e) {
          e.printStackTrace();
          
        }
        return null;
      }

      public static String encodeAccount(String email, String userId) {
        String encryptString = null;
        try {
          encryptString = EncryptDecrypt.encrypt(email +
        		  EncryptDecrypt.accountSeperator + userId);
        } catch (Exception e) {
        	System.out.println(e);  
        }
        return encryptString;
      }
      
      public static String   getEncryptedString(String code,String password){
    	  String str=code+"_"+"cashbackarab";
    	 
    	   return getMD5(getShai(str));
      }
      
      public static String  getMD5(String string) {
    		if(string == null) {
    			return null;
    		}
    		
    		return org.apache.commons.codec.digest.DigestUtils.md5Hex(string );
    	}
      
      public static String  getShai(String string) {
    		if(string == null) {
    			return null;
    		}
    		
    	return 	Hashing.sha1().hashString( string , Charsets.UTF_8 ).toString();
    	}

    public static void main(String args []) throws Exception
    {
    	EncryptDecrypt td= new EncryptDecrypt();

        String target="imparator";
        String encrypted=td.encrypt(target);
        String decrypted=td.decrypt(encrypted);

        System.out.println("String To Encrypt: "+ target);
        System.out.println("Encrypted String:" + encrypted);
        System.out.println("Decrypted String:" + decrypted);
        
    }
}
