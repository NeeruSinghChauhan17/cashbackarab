package com.cashback.cashbackarab.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cashback.cashbackarab.dao.impl.TransactionDaoImpl;
import com.cashback.cashbackarab.models.enums.CurrencyConverter;

public class GETCASHBACK {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionDaoImpl.class);
	
	public static Float getTotalCashback(Float amount,String currency,String cashback){
		if(cashback.contains("%")){
			cashback=cashback.replaceAll("%","");
			  String cash=CurrencyConverter.getCurrencyInToSR(currency);
			  LOGGER.info("old_cashback SR ====="+Float.valueOf(cash)*amount+" new_cashback SR====  "+ (  (amount*Float.valueOf(cashback))/100  )*Float.valueOf(cash));
			return ((  (amount*Float.valueOf(cashback))/100  )*Float.valueOf(cash));
		}
		else{
		   return  Float.valueOf(cashback);
		}
		
   }
}
