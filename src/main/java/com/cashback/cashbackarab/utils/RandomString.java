package com.cashback.cashbackarab.utils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class RandomString {
	
	private static final String MD5 = "MD5";
	private static final String SHA1 = "SHA-1";
	
	public static String generateRandomString(){
		char[] chars = "0123456789abcdefghijkmnpqrstvwxyzABCDEFGHJKLMNPQRTVWXYZ!(@)".toCharArray();
		String s = new String();
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    s=s+c;
		}	
		return s;
	}
	
	public static String generateRefrenceId(){
		char[] chars = "1239870456".toCharArray();
		String s = new String();
		Random random = new Random();
		for (int i = 0; i < 11; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    s=s+c;
		}	
		return s;
	}
	
	public static String MD5(final String input) {
		try {
			final MessageDigest messageDigest = MessageDigest.getInstance(MD5);
			final byte[] array = messageDigest.digest(input.getBytes());
			final StringBuilder builder = new StringBuilder();
			for (int i = 0; i < array.length; ++i) {
				builder.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return builder.toString();

		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException("Something went wrong while creating token");
		}
	}
	
	public static String SHA1(final String input) {
		try {
			final MessageDigest messageDigest = MessageDigest.getInstance(SHA1);
			final byte[] array = messageDigest.digest(input.getBytes());
			final StringBuilder builder = new StringBuilder();
			for (int i = 0; i < array.length; ++i) {
				builder.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return builder.toString();

		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException("Something went wrong while creating token");
		}
	}
	
	public static String encryptPassword(final String password) {
		System.out.println(MD5(SHA1(password)));
        return MD5(SHA1(password));
	}
	

	
	
	
	
	public static void main(String args[]){
		
		System.out.println(encryptPassword("12345"));
		
	}

}	