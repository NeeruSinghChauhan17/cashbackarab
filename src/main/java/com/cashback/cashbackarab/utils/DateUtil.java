package com.cashback.cashbackarab.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {

	private static final long SEC_IN_A_DAY = 60 * 60 * 24;
	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

	/**
	 * Return Epoch Date in Seconds without time. For Example : For date
	 * 28-12-2016 1:30:34 it return 28-12-2016 : 00:00:00 in epoch seconds.
	 * 
	 * @return
	 */

	public static Long getEpochDate() {
		return (Instant.now().getEpochSecond() / SEC_IN_A_DAY) * SEC_IN_A_DAY;
	}

	public static void main(String[] args) {
		// System.out.println(Instant.now().getEpochSecond());
		// System.out.println(getEpochDate());
		// System.out.println(LocalTime.now().toSecondOfDay());
		System.err.println(round(2.3, 2));
	}

	public static String convertToDateString(final Long seconds) {

		String dateString = null;
		SimpleDateFormat sdfr = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
		try {
			sdfr.setTimeZone(TimeZone.getTimeZone("IST"));
			dateString = sdfr.format(seconds * 1000);
		} catch (Exception ex) {
			LOGGER.info("error in date ",ex);
		}
		return dateString;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// dd/MM/yyyy
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}



	
	public static String getTime(Timestamp ts){
		Date date = new Date();
		date.setTime(ts.getTime());
		return new SimpleDateFormat("dd-MM-yyyy").format(date);
		
	}
	
	private static Date yesterday() {
	    final Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);
	    return cal.getTime();
	}
	
	public static String getYesterdayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY ");
        return dateFormat.format(yesterday());
}
}