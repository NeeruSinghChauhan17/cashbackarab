/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.cashback.cashbackarab.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class return UTC date and time.
 * 
 * @author Navrattan Yadav
 * 
 */
public class GMTCalender {

	private static final Logger _logger = LoggerFactory.getLogger(GMTCalender.class);
	public static final DateFormat YYYYMMDDHH_FORMAT = new SimpleDateFormat("yyyyMMddHH");

	private static GMTCalender _instance;
	private static int _offsetHrs;
	private static int _offsetMins;

	private GMTCalender() {
		TimeZone z = Calendar.getInstance().getTimeZone();
		int offset = z.getRawOffset();
		if (z.inDaylightTime(new Date())) {
			offset = offset + z.getDSTSavings();
		}
		_offsetHrs = offset / 1000 / 60 / 60;
		_offsetMins = offset / 1000 / 60 % 60;
		_logger.info("offset: " + _offsetHrs + ":" + _offsetMins);
	}

	public static synchronized GMTCalender getInstance() {
		if (_instance == null) {
			_instance = new GMTCalender();
		}
		return _instance;
	}

	public long getTime() {
		return getCalender().getTimeInMillis();
	}

	private Calendar getCalender() {
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.HOUR_OF_DAY, (-_offsetHrs));
		calender.add(Calendar.MINUTE, (-_offsetMins));
		return calender;
	}

	public long time(long time, int offset) {

		if (time != 0) {
			time = time + offset;
		}
		return time;
	}

}
