package com.cashback.cashbackarab.exception;

import javax.ws.rs.core.Response.Status;

public class UnAuthorizedException extends CustomException {

private static final long serialVersionUID = 1L;
	
	private static final Integer STATUS_UNAUTHORIZED = Status.UNAUTHORIZED.getStatusCode();

	public UnAuthorizedException() {
		super(STATUS_UNAUTHORIZED, "Aunautorized");
	}
	
	public UnAuthorizedException(final String message) {
		super(STATUS_UNAUTHORIZED,message);
	}

	public UnAuthorizedException(final String message, final Throwable cause) {
		super(STATUS_UNAUTHORIZED,message, cause);
	}
}
