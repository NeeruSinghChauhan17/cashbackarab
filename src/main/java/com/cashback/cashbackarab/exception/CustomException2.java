package com.cashback.cashbackarab.exception;

import org.springframework.http.HttpStatus;


import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomException2 extends Exception {

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final HttpStatus status ;
	
	private final ErrorEntity entity;

	public CustomException2(String message, HttpStatus status, Throwable cause, ErrorEntity entity) {
		super(message, cause);
		this.status = status;
		this.entity = entity;
	}

	@JsonProperty("status")
	public HttpStatus getStatus() {
		return status;
	}

	@JsonProperty("entity")
	public ErrorEntity getEntity() {
		return entity;
	}
}
