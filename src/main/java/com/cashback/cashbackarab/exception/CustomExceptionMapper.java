/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.cashback.cashbackarab.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CustomExceptionMapper implements ExceptionMapper<CustomException2> {

	public CustomExceptionMapper() {
	}

	@Override
	public Response toResponse(CustomException2 e) {
		return Response.status(e.getStatus().value())
				.entity(e.getEntity() != null ? e.getEntity() : e.getMessage())
				.type(MediaType.APPLICATION_JSON).build();
	}

}
