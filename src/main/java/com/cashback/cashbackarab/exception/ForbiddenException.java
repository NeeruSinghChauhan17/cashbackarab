package com.cashback.cashbackarab.exception;

import javax.ws.rs.core.Response.Status;

public class ForbiddenException extends CustomException {

	private static final long serialVersionUID = 1L;

	private static final Integer STATUS_FORBIDDEN = Status.FORBIDDEN.getStatusCode();

	public ForbiddenException() {
		super(STATUS_FORBIDDEN, "Conflict");
	}

	public ForbiddenException(final String message) {
		super(STATUS_FORBIDDEN, message);
	}

	public ForbiddenException(final String message, final Throwable cause) {
		super(STATUS_FORBIDDEN, message, cause);
	}
}
