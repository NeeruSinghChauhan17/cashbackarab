package com.cashback.cashbackarab.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Banner {
	
	private String title; 
	private String description;
	private Integer brandId;
	private String url;
	
	@SuppressWarnings("unused")
	private Banner(){
		this(null,null,null,null);
	}
	public Banner(final String title,String description,Integer brandId, final String url) {
		this.title = title;
		this.description=description;
		this.brandId=brandId;
		this.url = url;
		
	}
	
	
	public static Banner withBrandId(final String title,final String description,final String url){
		String[] des=description.split("=");
		Integer id=Integer.parseInt(des[1].replaceAll("\\s+",""));
		return new Banner(title,null,id,url);
	}
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	
	
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("brand_id")
	public Integer getBrandId() {
		return brandId;
	}
	@JsonProperty("url")
	public String getUrl() {
		return url;
	}
	
}
