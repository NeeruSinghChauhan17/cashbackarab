package com.cashback.cashbackarab.models;

public enum Action {
	
	ADD,REMOVE;
	public static int getIntValue(Action action) {
		switch (action) {
		case REMOVE:
			return 0;
		case ADD:
			return 1;
		default:
			return 0;
		}
	}
	
	public static Action getAction(int value) {
		switch (value) {
		case 0:
			return REMOVE;
		case 1:
			return ADD;
		default:
			return REMOVE;
		}
	}
	
	
}
