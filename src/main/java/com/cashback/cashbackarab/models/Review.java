package com.cashback.cashbackarab.models;

import javax.validation.constraints.NotNull;
import org.joda.time.DateTime;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Review {

	@NotNull
	private final Long brandId;
    private final String title;
    private final String text;

	@NotNull
	private final Float rating;

	private final Boolean status;

	private final String userName;

	private final String fname;

	private final String lname;

	private final DateTime timeStamp;

	@SuppressWarnings("unused")
	private Review() {
		this(null, null, null, null, null, null, null, null, null);
	}

	public Review(final Long brandId,final  String title,final  String text,final  Float rating,final  Boolean status,final  String userName,
			final String fname,final String lname, final DateTime timeStamp) {

		this.brandId = brandId;
		this.title = title;
		this.text = text;
		this.rating = rating;
		this.status = status;
		this.userName = userName;
		this.fname = fname;
		this.lname = lname;
		this.timeStamp = timeStamp;

	}

	@JsonProperty("brand_id")
	public Long getBrandId() {
		return brandId;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("text")
	public String getText() {
		return text;
	}

	@JsonProperty("rating")
	public Float getRating() {
		return rating;
	}

	@JsonProperty("status")
	public Boolean getStatus() {
		return status;
	}

	@JsonProperty("user_name")
	public String getUserName() {
		return userName;
	}

	@JsonProperty("first_name")
	public String getFname() {
		return fname;
	}

	@JsonProperty("last_name")
	public String getLname() {
		return lname;
	}

	@JsonProperty("timestamp")
	public DateTime getTimeStamp() {
		return timeStamp;
	}

}
