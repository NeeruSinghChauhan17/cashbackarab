package com.cashback.cashbackarab.models;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonInclude(Include.NON_NULL)
public class Login {
	
	//@NotBlank(message = "emailId can't be null or blank")
	@JsonProperty("email_id")
	private String emailId;
	
	
	@JsonProperty("phone_number")
	private  InternationalNumber number;

	@JsonProperty("password")
	@NotBlank(message = "password can't be null or blank")
	private String password;
	
	
	
	@SuppressWarnings("unused")
	private Login(){
		this(null,null,null);
	}
	
	
	
	public Login(final String emailId,final InternationalNumber number,final String password ){
		this.emailId=emailId;
		this.number=number;
		this.password=password;
	}
	
	
	public String getEmailId() {
		return emailId;
	}

	
	
	public InternationalNumber getNumber() {
		return number;
	}



	public String getPassword() {
		return password;
	}
	
}
