package com.cashback.cashbackarab.models;

public enum DeviceType {
	WEB, ANDROID, IOS, WINDOW, BLACKBERRY;
	public static int getIntValue(DeviceType deviceType) {
		switch (deviceType) {
		case WEB:
			return 0;
		case ANDROID:
			return 1;
		case IOS:
			return 2;
		case WINDOW:
			return 3;
		case BLACKBERRY:
			return 4;
		default:
			return 0;
		}
	}

	public static DeviceType getDeviceType(String value) {
		switch (value) {
		case "WEB":
			return WEB;
		case "ANDROID":
			return ANDROID;
		case "IOS":
			return IOS;
		case "WINDOW":
			return WINDOW;
		case "BLACKBERRY":
			return BLACKBERRY;
		default:
			return WEB;
		}
	}
}
