package com.cashback.cashbackarab.models.brand;

import java.sql.Timestamp;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Brand {
	private final Integer brandId;
	private final String title;
	private final String logo;
	private final String cashback;
	private final String description;
	private final String brandUrl;
	private final String metaDescription;
	private final String metaKeywords;
	private final DateTime endDate;
	private Integer dealsOfWeek;
	private String condition;
	private Timestamp added;
	private Boolean isFavourite;
	private Boolean isFeatured;
	private final Category category;
	private final String banner;
	private final Coupon coupon;

	@SuppressWarnings("unused")
	private Brand() {
		this(null, null, null, null, null, null, null, null, null, null,null, null, null, null, null, null, null);
	}

	public Brand(Integer brandId, String title, String logo, Category category, Integer dealsOfWeek, String cashback,
			String description, String brandUrl, String metaDescription, String metaKeywords, DateTime endDate,
			Boolean isFavourite,Boolean isFeatured, String condition, Timestamp added, String banner, final Coupon coupon) {
		super();
		this.brandId = brandId;
		this.title = title;
		this.logo = logo;
		this.cashback = cashback;
		this.description = description;
		this.brandUrl = brandUrl;
		this.metaDescription = metaDescription;
		this.metaKeywords = metaKeywords;
		this.endDate = endDate;
		this.dealsOfWeek = dealsOfWeek;
		this.condition = condition;
		this.added = added;
		this.isFavourite = isFavourite;
		this.isFeatured=isFeatured;
		this.category = category;
		this.banner = banner;
		this.coupon = coupon;
	}

	@JsonProperty("brand_id")
	public Integer getBranId() {
		return brandId;
	}

	@JsonProperty("brand_name")
	public String getTitle() {
		return title;
	}

	@JsonProperty("brand_url")
	public String getBrandUrl() {
		return brandUrl;
	}

	@JsonProperty("brand_logo")
	public String getImage() {
		return logo;
	}

	@JsonProperty("category")
	public Category getCategories() {
		return category;
	}

	@JsonProperty("deals_of_week")
	public Integer getDeals_of_week() {
		return dealsOfWeek;
	}

	@JsonProperty("cashback")
	public String getCashback() {
		return cashback;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("meta_description")
	public String getMeta_description() {
		return metaDescription;
	}

	@JsonProperty("meta_keywords")
	public String getMeta_keywords() {
		return metaKeywords;
	}

	@JsonProperty("end_date")
	public DateTime getEnd_date() {
		return endDate;
	}

	@JsonProperty("is_favourite")
	public Boolean isFavourite() {
		return isFavourite;
	}
	
	@JsonProperty("is_featured")
	public Boolean isFeatured() {
		return isFeatured;
	}
	

	@JsonProperty("condition")
	public String getCondition() {
		return condition;
	}

	@JsonProperty("added_date")
	public Timestamp getAdded() {
		return added;
	}

	@JsonProperty("banner")
	public String getBanner() {
		return banner;
	}

	@JsonProperty("coupon")
	public Coupon getCoupon() {
		return coupon;
	}

	public static Brand createWithId(final Integer brandId, final Brand brand) {
		return new Brand(brandId, null, null, null, null, null, null,null, null, null, null, null, null, null, null, null,
				null);
	}
}
