package com.cashback.cashbackarab.models.brand;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BrandList {

	@Valid
	@NotEmpty
	private List<Brand> brand;

	@SuppressWarnings("unused")
	private BrandList() {
		this(null);
	}

	public BrandList(final List<Brand> brand) {
		this.brand = brand;
	}

	@JsonProperty("brandList")
	public List<Brand> getBrand() {
		return brand;
	}

}
