package com.cashback.cashbackarab.models.brand;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Coupon {
	
	private final Integer coupon_id;
	private final String link;
	private final String code;
	private final String title;
	private final String description;
	private final DateTime startDate;
	private final DateTime endDate;
	private final Integer viewCount;
	
	@SuppressWarnings("unused")
	private Coupon(){
		this(null,null,null,null,null,null,null,null);
	}
	public Coupon(final Integer coupon_id, final String link,final  String code,final  String title,
			final String description,final DateTime startDate, final DateTime endDate, final Integer viewCount) {
		super();
		this.coupon_id = coupon_id;
		this.link = link;
		this.code = code;
		this.title = title;
		this.description=description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.viewCount = viewCount;
	}
	
	@JsonProperty("coupon_id")
	public Integer getCoupon_id() {
		return coupon_id;
	}
	
	@JsonProperty("coupon_link")
	public String getLink() {
		return link;
	}
	@JsonProperty("code")
	public String getCode() {
		return code;
	}
	
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("start_date")
	public DateTime getStart_date() {
		return startDate;
	}
	
	@JsonProperty("end_date")
	public DateTime getEnd_date() {
		return endDate;
	}
	
	@JsonProperty("view_count")
	public Integer getViewCount() {
		return viewCount;
	}
	
	
}
