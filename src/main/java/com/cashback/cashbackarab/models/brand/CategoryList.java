package com.cashback.cashbackarab.models.brand;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;


public class CategoryList {

	@Valid
	@NotEmpty
	private List<Category> category;

	@SuppressWarnings("unused")
	private CategoryList() {
		this(null);
	}

	public CategoryList(List<Category> category) {
		this.category = category;
	}

	public List<Category> getCategory() {
		return category;
	}

}
