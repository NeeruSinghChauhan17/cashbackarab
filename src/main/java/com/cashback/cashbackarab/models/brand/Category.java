package com.cashback.cashbackarab.models.brand;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Category {
	
	private final Integer categoryId;
	private final String name;
	private final String description;
	private final String categoryUrl;

	
	@SuppressWarnings("unused")
	private Category(){
		this(null,null,null,null);
	}
	
	public Category(final Integer categoryId, final String name, final String description,
			final String categoryUrl) {
		super();
		this.categoryId = categoryId;
		this.name = name;
		this.description = description;
		this.categoryUrl = categoryUrl;
	}
	
	@JsonProperty("category_id")
	public Integer getCategory_id() {
		return categoryId;
	}
	
	@JsonProperty("category_name")
	public String getName() {
		return name;
	}
	
	@JsonProperty("category_description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("category_url")
	public String getCategory_url() {
		return categoryUrl;
	}


}
