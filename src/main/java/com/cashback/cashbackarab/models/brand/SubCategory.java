package com.cashback.cashbackarab.models.brand;

public class SubCategory {
	private final String subCategoryId;
	private final String name;
	private final String description;
	private final String subCategoryUrl;
	
	@SuppressWarnings("unused")
	private SubCategory(){
		this(null,null,null,null);
	}

	public SubCategory(final String subCategoryId,final  String name,final  String description,
			final String subCategoryUrl) {
		super();
		this.subCategoryId = subCategoryId;
		this.name = name;
		this.description = description;
		this.subCategoryUrl = subCategoryUrl;
	}

	public String getSubCategoryId() {
		return subCategoryId;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getSubCategoryUrl() {
		return subCategoryUrl;
	}
	
	
}
