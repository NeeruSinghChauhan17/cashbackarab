package com.cashback.cashbackarab.models.brand;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShoppingTrips {
	private final Integer brandId; 
	private final String brandName;
	private final DateTime addedDate;
	
	@SuppressWarnings("unused")
	private ShoppingTrips(){
		this(null,null,null);
	}
	public ShoppingTrips(final Integer brandId, final String brandName, final DateTime addedDate) {
		super();
		this.brandId = brandId;
		this.brandName = brandName;
		this.addedDate=addedDate;
	}
	
	@JsonProperty("brand_id")
	public Integer getBrandId() {
		return brandId;
	}
	
	@JsonProperty("brand_name")
	public String getBrandName() {
		return brandName;
	}
	
	@JsonProperty("added_date")
	public DateTime getAddedDate() {
		return addedDate;
	}
	
	
}
