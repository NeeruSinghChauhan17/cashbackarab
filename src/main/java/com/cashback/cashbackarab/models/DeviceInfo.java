package com.cashback.cashbackarab.models;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class DeviceInfo {
	
	private final String deviceToken;
	private final DeviceType deviceType;
	
	@SuppressWarnings("unused")
	private DeviceInfo() {
		this(null ,null);
	}
	public DeviceInfo(final String deviceToken, final DeviceType deviceType) {
		this.deviceToken = deviceToken;
		this.deviceType  = deviceType;
	}
	
	@JsonProperty("device_token")
	public String getDeviceToken() {
		return deviceToken;
	}
	@JsonProperty("device_type")
	public DeviceType getDeviceType() {
		return deviceType;
	}

}
