package com.cashback.cashbackarab.models;

import com.cashback.cashbackarab.models.InternationalNumber;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UpdatePassword {

	
	@JsonProperty("phone_number")
	private final InternationalNumber number;
	
	
	@JsonProperty("code")
	private final String code;
	
	
	@JsonProperty("password")
	private final String password;
	
	
	@JsonProperty("string")
	private final String encryptString;
	
	
	@SuppressWarnings("unused")
	private UpdatePassword(){
		this(null,null,null,null);
	}
	
	public UpdatePassword(final InternationalNumber number,final String code,final String password,final String string){
		this.number=number;
		this.code=code;
		this.password=password;
		this.encryptString=string;
	}

	public InternationalNumber getNumber() {
		return number;
	}

	public String getCode() {
		return code;
	}

	public String getPassword() {
		return password;
	}

	public String getEncryptString() {
		return encryptString;
	}

	
	
	
	
	
}
