package com.cashback.cashbackarab.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonInclude(Include.NON_NULL)
public class User {

	@JsonProperty("user_id")
	private  final Integer userId;
	
	@JsonProperty("first_name")
	private final String fname;
	
	@JsonProperty("last_name")
	private final String lname;

	@JsonProperty("email_id")
	private  final String emailId;
	
	@JsonProperty("phone_number")
	private final InternationalNumber number;
	
	@JsonProperty("social_id")
	private  final String socialId;

	@JsonProperty("password")
	protected final String password;
	
	@JsonProperty("country")
	private final Integer country;
	
	@JsonProperty("is_verified")
	private final Integer isVerified; 
	
	@JsonProperty("is_number_verified")
	private final Integer isNoVerified;
	
	@JsonProperty("is_email_verified")
	private final Integer isEmailVerified;
	
	@JsonProperty("token")
	private final String token;
	
	@JsonProperty("status")
	private final String status;
	
	@JsonProperty("cashback_balance")
	private final Double cashbackBalance;
	
	@JsonProperty("profile_picture")
	private final String profilePicture;
	
	@JsonProperty("notification_state")
	private final Integer notificationState;
	
	
	
	
	
	@SuppressWarnings("unused")
	private User(){
		this(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
	}

	public User(final Integer userId, final String fname,final  String lname,final  String emailId,final InternationalNumber number,
			final String socialId ,final String password,Integer country ,final Integer isVerified ,final Integer isNoVerified,
			final Integer isEmailVerified,final String token, final String status,
			final Double cashbackBalance,final String profilePicture, final Integer notificationState) {
		super();
		this.userId = userId;
		this.fname = fname;
		this.lname = lname;
		this.emailId = emailId;
		this.number=number;
		this.socialId=socialId;
		this.password = password;
		this.country=country;
		this.isVerified=isVerified;
		this.isNoVerified=isNoVerified;
		this.isEmailVerified=isEmailVerified;
		this.token=token;
		this.status=status;
		this.cashbackBalance=cashbackBalance;
		this.profilePicture=profilePicture;
		this.notificationState=notificationState;
	}

	public Integer getUserId() {
		return userId;
	}

	public String getFname() {
		return fname;
	}

	public String getLname() {
		return lname;
	}

	public String getEmailId() {
		return emailId;
	}

	public InternationalNumber getNumber() {
		return number;
	}

	public String getPassword() {
		return password;
	}
	
	public Integer getCountry() {
		return country;
	}

	public String getToken() {
		return token;
	}
	
	public String getSocialId() {
		return socialId;
	}

	public Integer getIsVerified() {
		return isVerified;
	}
	
	public String getStatus() {
		return status;
	}
	
	public Double getCashbackBalance() {
		return cashbackBalance;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public Integer getNotificationState() {
		return notificationState;
	}

	public Integer getIsNoVerified() {
		return isNoVerified;
	}
	
	public Integer getIsEmailVerified() {
		return isEmailVerified;
	}
	public static User userWithId(User user,Integer id) {
		return new User(id,user.getFname(),user.getLname(),user.getEmailId(),null,null,user.getPassword(),user.getCountry(),
				null,null,null,null,null,null,null,null);
	}
	public static User userWithIdV2(User user,Integer id) {
		return new User(id,user.getFname(),user.getLname(),user.getEmailId(),new InternationalNumber(user.number.getCc(),user.number.getCode(),
			user.number.getNumber()),null,null,null,null,null,null,null,null,null,null,null);
	}
	
	

	public static User createWithToken(final String authToken, final User user) {
		return new User(user.getUserId(), user.getFname(), user.getLname(), user.getEmailId(),null,user.getSocialId(), user.getPassword()
				,user.getCountry(),null,null,null, authToken,null,null,null,null);
	}

	


	public static User createWithTokenWithNoPassword(String token, User user,Double cashbackBalance) {
		return new User(user.getUserId(),user.getFname(), user.getLname(),user.getEmailId()
				,null,
				null, null, user.getCountry(),null, null,null,token,null,cashbackBalance,
				user.getProfilePicture(), user.getNotificationState());
	}

	public static User createWithTokenWithNoPasswordV2(String token, User user,Double cashbackBalance) {
		return new User(user.getUserId(),user.getFname(), user.getLname(),user.getEmailId()
				,new InternationalNumber(user.number.getCc(),user.number.getCode(),user.number.getNumber()),
				null, null, null,null, user.isNoVerified,user.isEmailVerified,token,null,cashbackBalance,
				user.getProfilePicture(), user.getNotificationState());
	}
	public static User createWithTokenWithNoPasswordandWithNoNumberV2(String token, User user,Double cashbackBalance) {
		return new User(user.getUserId(),user.getFname(), user.getLname(),user.getEmailId()
				,null,
				null, null, null,null, null,user.isEmailVerified,token,null,cashbackBalance,
				user.getProfilePicture(), user.getNotificationState());
	}
	
	
}
