package com.cashback.cashbackarab.models.enums;

public enum CurrencyConverter {

	USD("3.75000"), EUR("4.47337"), JPY("0.034"), AUD("2.93"), CAD("3.01"), CHF("3.82"), SEK("0.46"), NZD("2.69"), MXN(
			"0.19"),RUB("0.066"),GBP("5.06"),ZAR("0.30");

	String currency;

	private CurrencyConverter(String currency) {
		this.currency = currency;
	}

	public String getCurrency() {
		return currency;
	}

	public static String getCurrencyInToSR(String currency) {
		switch (currency) {
		case "USD":
			return CurrencyConverter.USD.getCurrency();
		case "EUR":
			return CurrencyConverter.EUR.getCurrency();
		case "JPY":
			return CurrencyConverter.JPY.getCurrency();
		case "AUD":
			return CurrencyConverter.AUD.getCurrency();
		case "CAD":
			return CurrencyConverter.CAD.getCurrency();
        case "CHF":
			return CurrencyConverter.CHF.getCurrency();
		case "SEK":
			return CurrencyConverter.SEK.getCurrency();
		case "NZD":
			return CurrencyConverter.NZD.getCurrency();
		case "MXN":
			return CurrencyConverter.MXN.getCurrency();
		case "RUB":
			return CurrencyConverter.RUB.getCurrency();
		case "GBP":
			return CurrencyConverter.GBP.getCurrency();
		case "ZAR":
			return CurrencyConverter.ZAR.getCurrency();
		default:
			return null;
		}

	}

}
