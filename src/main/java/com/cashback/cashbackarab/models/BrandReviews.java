package com.cashback.cashbackarab.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class BrandReviews {

	private final Float averageRating;
	private final List<Review> reviews;

	@SuppressWarnings("unused")
	private BrandReviews() {
		this(null, null);
	}

	public BrandReviews(final Float averageRating,final  List<Review> reviews) {
		this.averageRating = averageRating;
		this.reviews = reviews;
	}

	public static BrandReviews withReviewAndAverageRating(final Float averageRating,final  List<Review> reviews) {
		return new BrandReviews(averageRating, reviews);
	}

	@JsonProperty("reviews")
	public List<Review> getReviews() {
		return reviews;
	}

	@JsonProperty("reviews_average_rating")
	public Float getAverageRating() {
		return averageRating;
	}

}
