package com.cashback.cashbackarab.models;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CashBack {
	
	private Double amount;
	
	private String cashbackType;
	
	private String status;
	private DateTime cashbackDate;
	
	@SuppressWarnings("unused")
	private CashBack(){
		this(null,null,null,null);
	}
	
	public CashBack(final Double amount, final String cashbackType, final String status,final DateTime cashbackDate){
		super();
		this.amount=amount;
		this.status=status;
		this.cashbackType=cashbackType;
		this.cashbackDate=cashbackDate;
	}

	@JsonProperty("amount")
	public Double getAmount() {
		return amount;
	}

	@JsonProperty("cashback_type")
	public String getCashbackType() {
		return cashbackType;
	}

	
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("cashback_date")
	public DateTime getCashbackDate() {
		return cashbackDate;
	}
	
}
