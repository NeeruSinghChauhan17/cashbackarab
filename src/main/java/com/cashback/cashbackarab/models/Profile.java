package com.cashback.cashbackarab.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Profile {
	
	
	private final Integer userId;
	
	@JsonProperty("first_name")
	private final String fname;
	
	@JsonProperty("last_name")
	private final String lname;
	
	@JsonProperty("email_id")
	private final String email;
	
	@JsonProperty("password")
	private final String password;
	
	@JsonProperty("phone")
	private final String phone;
	
	@JsonProperty("address")
	private final  String address;
	
	@JsonProperty("state")
	private final String state;
	
	@JsonProperty("city")
	private final String city;
	
	@JsonProperty("country")
	private final Integer country;
	
	@JsonProperty("pincode")
	private final String zip;	
	
	@JsonProperty("cashback_balance")
	private final Double cashbackBalance;
	
	@JsonProperty("profile_picture")
	private final String profilePicture;
	
	@SuppressWarnings("unused")
	private Profile(){
		this(null, null, null, null, null, null, null, null, null,null, null,null,null);
	}
	public Profile(final Integer userId,final String fname, final String lname,final String email,final String password, final  String phone,final  String address,
			final  String state,final String city,final Integer country,final  String zip, final Double cashbackBalance, final String profilePicture) {
		super();
		this.userId = userId;
		this.email = email;
		this.password=password;
		this.fname = fname;
		this.lname = lname;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.country = country;
		this.phone = phone;
		this.cashbackBalance = cashbackBalance;
		this.profilePicture = profilePicture;
	}
	
	public Integer getUser_id() {
		return userId;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getFname() {
		return fname;
	}
	
	public String getLname() {
		return lname;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getState() {
		return state;
	}
	
	
	public String getZip() {
		return zip;
	}
	
	
	public Integer getCountry() {
		return country;
	}
	
	public String getPhone() {
		return phone;
	}
	
	@JsonProperty("user_id")
	public Integer getUserId() {
		return userId;
	}
	
	public Double getCashbackBalance() {
		return cashbackBalance;
	}
	
	public String getProfilePicture() {
		return profilePicture;
	}
	
	public static Profile profileWithoutPassword(Integer userId,Profile user) {
		return new Profile(userId,user.getFname(), user.getLname(),user.getEmail(),null, user.getPhone(),user.getAddress(),
				user.getState(),user.getCity(),user.getCountry(),user.getZip(),null,user.getProfilePicture());
	}
	
	public static Profile profileWithCashbackBalances(Integer userId,Profile user, Double cashbackBalance) {
		return new Profile(userId,user.getFname(), user.getLname(),user.getEmail(),null, user.getPhone(),user.getAddress(),
				user.getState(),user.getCity(),user.getCountry(),user.getZip(),cashbackBalance,user.getProfilePicture());
	}
	
	public static Profile profileWithCashbackBalancesV2(Integer userId,Profile user, Double cashbackBalance) {
		return new Profile(userId,user.getFname(), user.getLname(),user.getEmail(),null, null,null,
				null,null,null,null,cashbackBalance,user.getProfilePicture());
	}
}
