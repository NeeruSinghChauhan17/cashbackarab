package com.cashback.cashbackarab.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
public class InternationalNumber {
	
	
	@JsonProperty("cc")
	private final String cc;
	
	
	@JsonProperty("iso_alpha_2_cc") // iso_country_code
	private final String code;
	
	@JsonProperty("number")
	private final String number;
	
	
	@SuppressWarnings("unused")
	private InternationalNumber(){
		this(null,null,null);
	}
		
		public InternationalNumber(final String cc,final String code,final String number){
			this.cc=cc;
			this.code=code;
			this.number=number;
			
		}

		public String getCc() {
			return cc;
		}

		public String getNumber() {
			return number;
		}

		public String getCode() {
			return code;
		}
	
		
		
		

}
