package com.cashback.cashbackarab.models.http;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Result {

	private final String comment;
	private final String clickUserIp;
	private final String currency;
	private final String websiteName;
	private final String statusUpdated;
	private final Long id;
	private final Long advcampaignId;
	private final String subId1;
	private final String subId2;
	private final String subId3;
	private final String subId4;
	private final String clickUserReferer;
	private final String clickDate;
	private final String actionId;
	private final String status;
	private final String orderId;
	private final Float cart;
	private final Long conversionTime;
	private final Long paid;
	private final Float payment;
	private final String advcampaignName;
	private final Long tariffId;
	private final String keyword;
	private final String closingDate;
	private final List<Positions> positions;
	private final String subId;
	private final String actionDate;
	private final Integer processed;
	private final String actionType;
	private final String action;

	@SuppressWarnings("unused")
	private Result() {
		this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null);
	}

	public Result(final String comment, final String clickUserIp, final String currency, final String websiteName,
			final String statusUpdated, final Long id, final Long advcampaignId, final String subId1,
			final String subId2, final String subId3, final String subId4, final String clickUserReferer,
			final String clickDate, final String actionId, final String status, final String orderId, final Float cart,
			final Long conversionTime,final  Long paid,final  Float payment,final String advcampaignName, final Long tariffId,
			final String keyword,final String closingDate, final List<Positions> positions, final String subId, final String actionDate,
			final Integer processed, final String actionType, final String action) {
		this.comment = comment;
		this.clickUserIp = clickUserIp;
		this.currency = currency;
		this.websiteName = websiteName;
		this.statusUpdated = statusUpdated;
		this.id = id;
		this.advcampaignId = advcampaignId;
		this.subId1 = subId1;
		this.subId2 = subId2;
		this.subId3 = subId3;
		this.subId4 = subId4;
		this.clickUserReferer = clickUserReferer;
		this.clickDate = clickDate;
		this.actionId = actionId;
		this.status = status;
		this.orderId = orderId;
		this.cart = cart;
		this.conversionTime = conversionTime;
		this.paid = paid;
		this.payment = payment;
		this.advcampaignName = advcampaignName;
		this.tariffId = tariffId;
		this.keyword = keyword;
		this.closingDate = closingDate;
		this.positions = positions;
		this.subId = subId;
		this.actionDate = actionDate;
		this.processed = processed;
		this.actionType = actionType;
		this.action = action;

	}

	@JsonProperty("comment")
	public String getComment() {
		return comment;
	}

	@JsonProperty("click_user_ip")
	public String getClickUserIp() {
		return clickUserIp;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	@JsonProperty("website_name")
	public String getWebsiteName() {
		return websiteName;
	}

	@JsonProperty("status_updated")
	public String getStatusUpdated() {
		return statusUpdated;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("advcampaign_id")
	public Long getAdvcampaignId() {
		return advcampaignId;
	}

	@JsonProperty("subid1")
	public String getSubId1() {
		return subId1;
	}

	@JsonProperty("subid2")
	public String getSubId2() {
		return subId2;
	}

	@JsonProperty("subid3")
	public String getSubId3() {
		return subId3;
	}

	@JsonProperty("subid4")
	public String getSubId4() {
		return subId4;
	}


	@JsonProperty("click_user_referer")
	public String getClickUserReferer() {
		return clickUserReferer;
	}

	@JsonProperty("click_date")
	public String getClickDate() {
		return clickDate;
	}

	@JsonProperty("action_id")
	public String getActionId() {
		return actionId;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("order_id")
	public String getOrderId() {
		return orderId;
	}

	@JsonProperty("cart")
	public Float getCart() {
		return cart;
	}

	@JsonProperty("conversion_time")
	public Long getConversionTime() {
		return conversionTime;
	}

	@JsonProperty("paid")
	public Long getPaid() {
		return paid;
	}

	@JsonProperty("payment")
	public Float getPayment() {
		return payment;
	}

	@JsonProperty("advcampaign_name")
	public String getAdvcampaignName() {
		return advcampaignName;
	}

	@JsonProperty("tariff_id")
	public Long getTariffId() {
		return tariffId;
	}

	@JsonProperty("keyword")
	public String getKeyword() {
		return keyword;
	}

	@JsonProperty("closing_date")
	public String getClosingDate() {
		return closingDate;
	}

	@JsonProperty("positions")
	public List<Positions> getPositions() {
		return positions;
	}


	@JsonProperty("subid")
	public String getSubId() {
		return subId;
	}

	@JsonProperty("action_date")
	public String getActionDate() {
		return actionDate;
	}

	@JsonProperty("processed")
	public Integer getProcessed() {
		return processed;
	}

	@JsonProperty("action_type")
	public String getActionType() {
		return actionType;
	}

	@JsonProperty("action")
	public String getAction() {
		return action;
	}
	
	
	

}
