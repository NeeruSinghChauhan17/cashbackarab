package com.cashback.cashbackarab.models.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Meta {
	

	private final Integer count;
	private final Integer limit;
	private final Integer offset;

	@SuppressWarnings("unused")
	private Meta() {
		this(null, null, null);
	}

	public Meta(final Integer count,final  Integer limit,final  Integer offset) {
		this.count = count;
		this.limit = limit;
		this.offset = offset;

	}

	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	@JsonProperty("limit")
	public Integer getLimit() {
		return limit;
	}

	@JsonProperty("offset")
	public Integer getOffset() {
		return offset;
	}

}
