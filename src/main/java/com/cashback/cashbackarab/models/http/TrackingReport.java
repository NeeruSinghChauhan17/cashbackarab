package com.cashback.cashbackarab.models.http;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TrackingReport {

	private List<Result> results;
	private Meta meta;

	@SuppressWarnings("unused")
	private TrackingReport() {
		this(null, null);
	}

	public TrackingReport(final List<Result> results, final Meta meta) {
		this.results = results;
		this.meta = meta;
	}

	@JsonProperty("results")
	public List<Result> getResults() {
		return results;
	}

	@JsonProperty("_meta")
	public Meta getMeta() {
		return meta;
	}
}
