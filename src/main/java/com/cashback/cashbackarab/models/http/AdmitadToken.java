package com.cashback.cashbackarab.models.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AdmitadToken {

	private final String userName;
	private final String firstName;
	private final String lastName;
	private final String language;
	private final String accessToken;
	private final Long expiresIn;
	private final String tokenType;
	private final String scope;
	private final Long id;
	private final String refreshToken;

	@SuppressWarnings("unused")
	private AdmitadToken() {
		this(null, null, null, null, null, null, null, null, null, null);
	}

	public AdmitadToken(final String userName, final String firstName, final String lastName, final String language,
			final String accessToken, final Long expiresIn, final String tokenType, final String scope, final Long id,
			final String refreshToken) {
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.language = language;
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
		this.tokenType = tokenType;
		this.scope = scope;
		this.id = id;
		this.refreshToken = refreshToken;

	}

	@JsonProperty("username")
	public String getUserName() {
		return userName;
	}

	@JsonProperty("first_name")
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty("last_name")
	public String getLastName() {
		return lastName;
	}

	@JsonProperty("language")
	public String getLanguage() {
		return language;
	}

	@JsonProperty("access_token")
	public String getAccessToken() {
		return accessToken;
	}

	@JsonProperty("expires_in")
	public Long getExpiresIn() {
		return expiresIn;
	}

	@JsonProperty("token_type")
	public String getTokenType() {
		return tokenType;
	}

	@JsonProperty("scope")
	public String getScope() {
		return scope;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("refresh_token")
	public String getRefreshToken() {
		return refreshToken;
	}

}
