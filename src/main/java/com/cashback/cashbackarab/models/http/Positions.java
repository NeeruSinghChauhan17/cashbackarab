package com.cashback.cashbackarab.models.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Positions {

	

	private final Long tarrifId;
	private final String productId;
	private final String productName;
	private final String amount;
	private final String dateTime;
	private final String productUrl;
	private final String rate;
	private final String productCategoryId;
	private final String productCategoryName;
	private final String id;
	private final String rateId;
	private final String percentage;
	private final String payment;
	private final String productImage;

	@SuppressWarnings("unused")
	private Positions() {
		this(null, null, null, null, null, null,null, null, null, null, null, null, null, null);
	}

	public Positions(final Long tarrifId,final  String productId, final String productName,final  String amount,
			final  String dateTime,final	String productUrl,final String rate, final String productCategoryId, 
			final String productCategoryName,final String id,final  String rateId,final  String percentage,
		final String payment,final  String productImage) {
		this.tarrifId = tarrifId;
		this.productId = productId;
		this.productName = productName;
		this.amount = amount;
		this.dateTime = dateTime;
		this.productUrl = productUrl;
		this.rate = rate;
		this.productCategoryId = productCategoryId;
		this.productCategoryName=productCategoryName;
		this.id = id;
		this.rateId = rateId;
		this.percentage = percentage;
		this.payment = payment;
		this.productImage = productImage;

	}

	@JsonProperty("tariff_id")
	public Long getTarrifId() {
		return tarrifId;
	}

	@JsonProperty("product_id")
	public String getProductId() {
		return productId;
	}

	@JsonProperty("product_name")
	public String getProductName() {
		return productName;
	}

	@JsonProperty("amount")
	public String getAmount() {
		return amount;
	}

	@JsonProperty("datetime")
	public String getDateTime() {
		return dateTime;
	}

	@JsonProperty("product_url")
	public String getProductUrl() {
		return productUrl;
	}

	

	@JsonProperty("rate")
	public String getRate() {
		return rate;
	}

	@JsonProperty("product_category_id")
	public String getProductCategoryId() {
		return productCategoryId;
	}

	@JsonProperty("product_category_name")
	public String getProductCategoryName() {
		return productCategoryName;
	}
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("rate_id")
	public String getRateId() {
		return rateId;
	}

	@JsonProperty("percentage")
	public String getPercentage() {
		return percentage;
	}

	@JsonProperty("payment")
	public String getPayment() {
		return payment;
	}

	@JsonProperty("product_image")
	public String getProductImage() {
		return productImage;
	}

}
