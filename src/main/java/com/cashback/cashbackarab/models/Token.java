package com.cashback.cashbackarab.models;

import java.util.UUID;

import org.joda.time.DateTime;

public class Token {

	private final Integer userId;

	private final String uniqueId;

	private final DateTime creationDateTime;

	@SuppressWarnings("unused")
	private Token() {
		this(null, null, null);
	}

	public Token(final Integer userId,final  String uniqueId,final  DateTime creationDateTime) {
		this.userId = userId;
		this.uniqueId = uniqueId;
		this.creationDateTime = creationDateTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public DateTime getCreationDateTime() {
		return creationDateTime;
	}

	public static Token createToken(final Integer userId) {
		return new Token(userId, UUID.randomUUID().toString(), DateTime.now());
	}

}
