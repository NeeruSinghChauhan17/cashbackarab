package com.cashback.cashbackarab.models;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChangePassword {
	@NotNull
	@NotBlank
	@JsonProperty("old_password")
	private String oldPassword;
	
	@NotNull
	@NotBlank
	@JsonProperty("new_password")
	private String newPassword;

	
	public String getOldPassword() {
		return oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}
	
	
}
