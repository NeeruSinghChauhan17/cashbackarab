package com.cashback.cashbackarab.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Setting {

	private final String key;
	private final Integer value;

	@SuppressWarnings("unused")
	private Setting() {
		this(null, null);
	}

	public Setting(String key, Integer value) {
		this.key = key;
		this.value = value;
	}

	@JsonProperty("key")
	public String getKey() {
		return key;
	}

	@JsonProperty("value")
	public Integer getValue() {
		return value;
	}

}
