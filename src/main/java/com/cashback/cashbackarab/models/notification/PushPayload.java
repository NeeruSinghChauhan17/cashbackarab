package com.cashback.cashbackarab.models.notification;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class PushPayload {

	private final String title;
	private final String body;
	private final Boolean customPayload;
	private final Map<String, String> customData;
	
	
	@SuppressWarnings("unused")
	private PushPayload() {
		this(null, null, null, null);
	}
	
	
	public PushPayload(final String title, final String body, final Boolean customPayload,
			final Map<String, String> customData) {
		this.title = title;
		this.body = body;
		this.customPayload = customPayload;
		this.customData = customData;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("body")
	public String getBody() {
		return body;
	}
	
	@JsonProperty("is_custom_payload")
	public Boolean isCustomPayload() {
		return customPayload;
	}

	@JsonProperty("custom_data")
	public Map<String, String> getCustomData() {
		return customData;
	}
	
   public static PushPayload withUserCashbackPayload(Map<String, String> customData){
	 return new PushPayload(null,null ,true,customData);
	}
	
	public static PushPayload withCustomData(Boolean isCustomData,Map<String, String> customData){
		return new PushPayload(null,null,isCustomData,customData);
	}
	
	
	
}

