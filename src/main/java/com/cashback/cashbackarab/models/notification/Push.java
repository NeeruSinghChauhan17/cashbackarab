package com.cashback.cashbackarab.models.notification;

import java.util.List;

import com.cashback.cashbackarab.models.DeviceInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Push {

	private final Long senderId;  // unique for every project at firebase
	private final List<DeviceInfo> devices;
	private final PushPayload payload;
	
	@SuppressWarnings("unused")
	private Push(){
		this(null, null, null);
	}
	
	public Push(final Long senderId, final List<DeviceInfo> devices, final PushPayload payload) {
		this.senderId = senderId;
		this.devices = devices;
		this.payload = payload;
	}

	@JsonProperty("devices")
	public List<DeviceInfo> getDevices() {
		return devices;
	}

	@JsonProperty("payload")
	public PushPayload getPayload() {
		return payload;
	}
	
	@JsonProperty("sender_id")
	public Long getSenderId(){
		return senderId;
	}
	
	public static Push createWithSenderIdAndDevices(Long senderId, final List<DeviceInfo> devices, final Push push) {
		return new Push(senderId, devices, push.getPayload());
	}
	
	public static Push withPayload(PushPayload payload){
		return new Push(null,null,payload);
	}
	
	
}
