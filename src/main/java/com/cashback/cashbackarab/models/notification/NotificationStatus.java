package com.cashback.cashbackarab.models.notification;

public enum NotificationStatus {

	ACTIVE,DEACTIVE;
	public static int getIntValue(NotificationStatus notificationStatus) {
		switch (notificationStatus) {
		case DEACTIVE:
			return 0;
		case ACTIVE:
			return 1;
		default:
			return 0;
		}
	}
	
	public static NotificationStatus getNotificationStatus(int value) {
		switch (value) {
		case 0:
			return DEACTIVE;
		case 1:
			return ACTIVE;
		default:
			return DEACTIVE;
		}
	}
}
