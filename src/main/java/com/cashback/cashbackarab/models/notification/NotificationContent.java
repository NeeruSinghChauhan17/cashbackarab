package com.cashback.cashbackarab.models.notification;

public enum NotificationContent {

	NEW_TRENDING_DEALS,CASHBACK_INCREASES,SPECIAL_SALE_OR_OFFER,NO_CONTENT,SEND_USER_CHECK_PAYMENT,USER_CASHBACK;
	public static NotificationContent getIntValue(String key) {
		switch (key) {
		case "1":
			return NEW_TRENDING_DEALS;
		case "2":
			return CASHBACK_INCREASES;
		case "3":
			return SPECIAL_SALE_OR_OFFER;
		case "4":
			return SEND_USER_CHECK_PAYMENT;
		case "5" :
			return USER_CASHBACK;
	   default:
		    return NO_CONTENT;
		}
	}
	
}
