package com.cashback.cashbackarab.models.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RedeemCashback {
	private final Integer paymentMethod;
	private final String paymentDetails;
	private final Double amount;
	
	@SuppressWarnings("unused")
	private RedeemCashback(){
		this(null,null,null);
	}
	
	public RedeemCashback(final Integer paymentMethod ,final String paymentDetails,final  Double amount) {
		super();
		this.paymentMethod = paymentMethod;
		this.paymentDetails = paymentDetails;
		this.amount=amount;
		
	}
    
	@JsonProperty("payment_method")
	public Integer getPaymentMethod() {
		return paymentMethod;
	}

	@JsonProperty("payment_details")
	public String getPaymentDetails() {
		return paymentDetails;
	}

	@JsonProperty("amount")
	public Double getAmount() {
		return amount;
	}
	
	
}
