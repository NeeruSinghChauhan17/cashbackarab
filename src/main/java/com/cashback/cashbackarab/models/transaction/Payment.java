package com.cashback.cashbackarab.models.transaction;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Payment {

	private final Integer paymentId;
	private final String paymentType;
	private final String paymentDetails;
	
	@SuppressWarnings("unused")
	private Payment(){
		this(null,null,null);
	}
	
	public Payment(final Integer paymentId, final String paymentType,final  String paymentDetails) {
		super();
		this.paymentId = paymentId;
		this.paymentType = paymentType;
		this.paymentDetails = paymentDetails;
		
	}

	@JsonProperty("payment_id")
	public Integer getPaymentId() {
		return paymentId;
	}

	@JsonProperty("payment_type")
	public String getPaymentType() {
		return paymentType;
	}

	@JsonProperty("payment_details")
	public String getPaymentDetails() {
		return paymentDetails;
	}


}
