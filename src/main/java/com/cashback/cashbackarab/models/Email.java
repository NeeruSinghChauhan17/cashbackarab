package com.cashback.cashbackarab.models;

public class Email {

		private final String language;
		private final String emailName;
		private final String emailSubject;
		private final String emailMessage;
		
		@SuppressWarnings("unused")
		private Email(){
			this(null,null,null,null);
		}
		
		public Email(final String language, final String emailName, final String emailSubject,final String emailMessage){
			super();
			this.language=language;
			this.emailName=emailName;
			this.emailSubject=emailSubject;
			this.emailMessage=emailMessage;
		}

		public String getLanguage() {
			return language;
		}

		public String getEmailName() {
			return emailName;
		}

		public String getEmailSubject() {
			return emailSubject;
		}

		public String getEmailMessage() {
			return emailMessage;
		}
		
}
