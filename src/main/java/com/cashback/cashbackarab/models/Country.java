package com.cashback.cashbackarab.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Country {

	
	private Integer countryId;
	
	
	private String countryCode;
	
	
	private String CountryName;
	
	@SuppressWarnings("unused")
	private Country(){
		this(null,null,null);
	}
	
	public Country(final Integer countryId, final String countryCode, final String CountryName){
		super();
		this.countryId=countryId;
		this.countryCode=countryCode;
		this.CountryName=CountryName;
	}

	@JsonProperty("country_id")
	public Integer getCountryId() {
		return countryId;
	}

	@JsonProperty("country_code")
	public String getCountryCode() {
		return countryCode;
	}

	@JsonProperty("country_name")
	public String getCountryName() {
		return CountryName;
	}
	
}
