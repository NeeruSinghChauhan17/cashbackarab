package com.cashback.cashbackarab.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("cashback/api/v1/content")
public interface ContentResource {

	@Path("/firebase/users/{user_id}/customtoken")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	Response getFirebaseAuthenticationToken(@PathParam("user_id") final  Integer userId);
	
		
}
