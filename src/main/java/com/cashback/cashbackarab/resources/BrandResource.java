package com.cashback.cashbackarab.resources;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cashback.cashbackarab.models.Action;
import com.cashback.cashbackarab.models.Review;

@Path("/cashback/api/v1/brand")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface BrandResource {

	@GET
	@Path("/trendingdeals/country/{country_id}") // not used
	Response getTrendingDeals( Integer userId, @PathParam("country_id") final  Integer countryId);      //no need userId

	@GET
	@Path("/getalltrendingdeals")     // Done 
	Response getAllTrendingDeals();

	@GET
	@Path("/user/{user_id}/trendingstores/{country_id}")      //not used
	Response getTrendingStores(@PathParam("user_id") Integer userId, @PathParam("country_id") final Integer countryId);

	@GET
	@Path("/getalltrendingstores")      // Done
	Response getAllTrendingStores(@QueryParam("user_id") @DefaultValue("0")final  Integer userId);

	@GET
	@Path("/{brand_id}/samecategory")               // Done
	Response getSameCategoryBrands(@QueryParam("user_id") @DefaultValue("0") final  Integer userId, @PathParam("brand_id") final Integer brandId);

	@GET
	@Path("/{brand_id}/getcoupons")                    // Done   
	Response getCouponsByBrandId( Integer userId, @PathParam("brand_id") final  Integer brandId);   //no need of userId

	@GET
	@Path("/coupons")      // Done
	Response getCoupons(); 
	
	@GET
	@Path("/country/{country_id}/coupons")                // not used
	Response getCouponsByCountry(@PathParam("country_id") final Integer countryId);        

	@GET
	@Path("/category/{category_id}/coupons")                   // Done
	Response getCouponsByCategory(@PathParam("category_id") final Integer categoryId); 

	@GET
	@Path("/category/{category_id}/country/{country_id}/coupons")                   // not used
	Response getCouponsByCategoryAndCountry(@PathParam("category_id") final  Integer categoryId,
			@PathParam("country_id") final Integer countryId);

	@GET
	@Path("/featureddeals")  //featured Deals or Hot Deals           //not used
	Response getFeaturedDeals(); 
    
	@PUT
	@Path("user/{user_id}/activatedeal")                //Done
	Response activateDeal(@PathParam("user_id") final Integer userId, @QueryParam("brand_id") final  Integer brandId,
			@QueryParam("coupon_id")final  Integer couponId);  

	@GET
	@Path("/{brand_id}/branddetails")       // Done
	Response getBrandDetails(@QueryParam("user_id") @DefaultValue("0") final Integer userId, @PathParam("brand_id") final  Integer brandId); 

	@GET
	@Path("/getdetailsbystring/{brand_name}")        //Done
	Response searchDeals(@QueryParam("user_id") @DefaultValue("0")final  Integer userId, @PathParam("brand_name") final String brandName); //

	@PUT
	@Path("/{brand_id}/user/{user_id}/favourite/{status}")                //Done
	Response addOrRemoveStoreFromFavourite(@PathParam("brand_id") final Integer brandId, @PathParam("user_id") final Integer userId,
			@PathParam("status") Action action);

	@GET
	@Path("/user/{user_id}/favourite")                 //Done
	Response getFavouriteBrands(@PathParam("user_id") final Integer userId);

	@GET
	@Path("/user/{user_id}/shoppingtrips")             //Done
	Response getShoppingTrips(@PathParam("user_id") final  Integer userId);
	
	@Path("/bannerimages")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	Response getBannerImages();         //Done

	@GET
	@Path("/{brand_id}/reviews")
	Response getReviewsByBrandId(@PathParam("brand_id") final Long brandId);

	@POST
	@Path("user/{user_id}/addreview")
	Response addReview(@PathParam("user_id") final  Integer userId,@Valid final Review review);
}
