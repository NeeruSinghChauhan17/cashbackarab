package com.cashback.cashbackarab.resources;

import static com.cashback.cashbackarab.provider.filter.HeaderParam.DEVICE_TOKEN;
import static com.cashback.cashbackarab.provider.filter.HeaderParam.DEVICE_TYPE;
import static com.cashback.cashbackarab.provider.filter.HeaderParam.USER_AGENT;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cashback.cashbackarab.models.ChangePassword;
import com.cashback.cashbackarab.models.Login;
import com.cashback.cashbackarab.models.Profile;
import com.cashback.cashbackarab.models.User;
import com.cashback.cashbackarab.models.notification.NotificationStatus;;

@Path("/cashback/api/v1/users")
public interface UserResource {

	static final String encoding = "utf-8";

	@PUT
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response register(User user, @HeaderParam(USER_AGENT) String userAgent,
			@NotNull(message = "device token can't be null or blank") @HeaderParam(DEVICE_TOKEN) String deviceToken,
			@NotNull(message = "device type can't be null or blank") @HeaderParam(DEVICE_TYPE) String deviceType,
			@QueryParam("ref") Integer refId);

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response login(@Valid @NotNull Login login, @HeaderParam(USER_AGENT) String userAgent,
			@NotNull(message = "device token can't be null or blank") @HeaderParam(DEVICE_TOKEN) String deviceToken,
			@NotNull(message = "device type can't be null or blank") @HeaderParam(DEVICE_TYPE) String deviceType);
     
	@GET
	@Path("/sociallogin/{social_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response socialLogin(@Valid @NotNull @PathParam("social_id") String socialId,
			@HeaderParam(USER_AGENT) String userAgent,
			@NotNull(message = "device token can't be null or blank") @HeaderParam(DEVICE_TOKEN) String deviceToken,
			@NotNull(message = "device type can't be null or blank") @HeaderParam(DEVICE_TYPE) String deviceType);

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{user_id}/verify")
	Response verifyEmail(@PathParam("user_id") Long userId);

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/forgot/{email_id}")
	Response forgotPassword(@PathParam("email_id") String emailId);

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/resendVerficationMail/{email_id}")
	Response resendVerficationMail(@PathParam("email_id") String emailId);

	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=" + encoding)
	@Path("/getAllCountries")
	Response getAllCountries();

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/getEmailBySocialId/{social_id}")
	Response getEmailBySocialId(@PathParam("social_id") String socialId);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{user_id}/getbasicprofile")
	Response getProfileById(@PathParam("user_id") Integer userId);

	@PUT
	@Path("/{user_id}/updateprofile")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response updateUserProfile(@PathParam("user_id") Integer userId, Profile userProfile);
    
	@POST
	@Path("/{user_id}/changepassword")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response updatePassword(@PathParam("user_id") Integer userId, ChangePassword changePassword,
			@HeaderParam(USER_AGENT) String userAgent,
			@NotNull(message = "device type can't be null or blank") @HeaderParam(DEVICE_TYPE) String deviceType);

	@PUT
	@Path("/{user_id}/notification/{status}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response updateNotificationStatus(@PathParam("user_id") Integer userId,
			@PathParam("status") NotificationStatus status);
    
	@PUT
	@Path("/{user_id}/updatepushtoken/{device_token}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response updatePushToken(@PathParam("user_id") Integer userId, @PathParam("device_token") String deviceToken,
			@NotNull(message = "device type can't be null or blank") @HeaderParam(DEVICE_TYPE) String deviceType);

	@PUT
	@Path("/{user_id}/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response logout(@PathParam("user_id") Integer userId, @HeaderParam(USER_AGENT) String userAgent,
			@NotNull(message = "device type can't be null or blank") @HeaderParam(DEVICE_TYPE) String deviceType);

	@PUT
	@Path("/{user_id}/updateprofilepic")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response updateProfilePic(@PathParam("user_id") Integer userId, Map<String, String> pictureUrl);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{user_id}/getcashbackhistory")
	Response getCashbackHistory(@PathParam("user_id") Integer userId);

	@GET
	@Path("/getsettingvalues")
	Response getSettingValues();  //removed userId(no need)
}
