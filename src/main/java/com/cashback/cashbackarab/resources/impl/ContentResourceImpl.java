package com.cashback.cashbackarab.resources.impl;

import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.cashback.cashbackarab.annotation.Secured;
import com.cashback.cashbackarab.content.FirebaseCustomToken;
import com.cashback.cashbackarab.resources.ContentResource;
import com.cashback.cashbackarab.service.ContentService;

@Controller
public class ContentResourceImpl implements ContentResource {
	private final ContentService contentService;

	@Autowired
	public ContentResourceImpl(final ContentService contentService) {
		this.contentService = contentService;
	}

	@Secured
	@Override
	public Response getFirebaseAuthenticationToken(final Integer userId) {

		final Optional<FirebaseCustomToken> opToken = contentService.getFirebaseCustomToken(userId);
		if (!opToken.isPresent()) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		return Response.status(Status.OK).entity(opToken.get()).build();
	}

}
