package com.cashback.cashbackarab.resources.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cashback.cashbackarab.annotation.Secured;
import com.cashback.cashbackarab.models.Action;
import com.cashback.cashbackarab.models.Banner;
import com.cashback.cashbackarab.models.BrandReviews;
import com.cashback.cashbackarab.models.Review;
import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.models.brand.Coupon;
import com.cashback.cashbackarab.models.brand.ShoppingTrips;
import com.cashback.cashbackarab.resources.BrandResource;
import com.cashback.cashbackarab.service.BrandService;


@Component
public class BrandResourceImpl implements BrandResource {

	private final BrandService brandService;

	@Autowired
	public BrandResourceImpl(final BrandService brandService) {
		this.brandService = brandService;
	}

	//@Secured                                //no need
	@Override
	public Response getTrendingDeals(final Integer userId, final Integer countryId) {

		List<Brand> brand = brandService.getTrendingDetails(userId, countryId);
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

	// @Secured                        //no need
	@Override
	public Response getAllTrendingDeals() {
		List<Brand> brand = brandService.getAllTrendingDetails();
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

	@Secured
	@Override
	public Response getTrendingStores(final Integer userId, final Integer countryId) {
		List<Brand> brand = brandService.getTrendingStores(userId, countryId);
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

	 @Secured
	@Override
	public Response getAllTrendingStores(final Integer userId) {
		
		List<Brand> brand = brandService.getAllTrendingStores(userId);
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

    @Secured
	@Override
	public Response getSameCategoryBrands(final Integer userId,final Integer brandId) {
		List<Brand> brand = brandService.getSameCategoryBrands(userId, brandId);
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

	//@Secured                               //no need
	@Override
	public Response getCouponsByBrandId(final Integer userId, final Integer brandId) {
		List<Coupon> couponList = brandService.getCouponsByBrandId(userId, brandId);
		if (couponList.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(couponList).build();
	}

	// @Secured                          //no need
	@Override
	public Response getCoupons() {
		List<Brand> couponList = brandService.getCoupons();
		if (couponList.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(couponList).build();
	}

	//@Secured                      //no need
	@Override
	public Response getCouponsByCountry(final Integer countryId) {
		List<Brand> couponList = brandService.getCouponsByCountry(countryId);
		if (couponList.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(couponList).build();
	}

	// @Secured                          //no need
	@Override
	public Response getCouponsByCategory(final Integer categoryId) {
		List<Brand> couponList = brandService.getCouponsByCategory(categoryId);
		if (couponList.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(couponList).build();
	}

	//@Secured                   //no need
	@Override
	public Response getCouponsByCategoryAndCountry(final Integer categoryId,final Integer countryId) {
		List<Brand> couponList = brandService.getCouponsByCategoryAndCountry(categoryId, countryId);
		if (couponList.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(couponList).build();
	}

	//@Secured         //no need
	@Override
	public Response getFeaturedDeals() {
		List<Brand> brand = brandService.getFeaturedDeals();
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

	@Secured
	@Override
	public Response activateDeal(final Integer userId, final Integer brandId, final Integer couponId) {
		brandService.activateDeal(userId, brandId, couponId);
		return Response.status(Status.NO_CONTENT).build();
	}

     @Secured
	@Override
	public Response getBrandDetails(Integer userId, final Integer brandId) {
		
		final Optional<Brand> brand = brandService.getBrandDeatils(userId, brandId);
		if (brand.isPresent()) {
			return Response.ok().entity(brand.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	//@Secured                         //no need
	@Override
	public Response searchDeals(final Integer userId, final String brandName) {
		Map<String, Object> brand = brandService.getSearchDetails(userId, brandName);
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

	@Secured
	@Override
	public Response addOrRemoveStoreFromFavourite(final Integer brandId, final Integer userId, final Action action) {
	   brandService.addOrRemoveStoreFromFavourite(brandId, userId, action);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response getFavouriteBrands(final Integer userId) {
		List<Brand> brand = brandService.getFavouriteBrands(userId);
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

	@Secured
	@Override
	public Response getShoppingTrips(final Integer userId) {
		List<ShoppingTrips> shoppingTrips = brandService.getShoppingTrips(userId);
		if (shoppingTrips.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(shoppingTrips).build();
	}

	@Override
	public Response getBannerImages() {
		List<Banner> banners = brandService.getBannerImages();
		if (banners.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(banners).build();
	}
	
	@Secured
	@Override	public Response addReview( final Integer userId,final Review review) {
		brandService.addReview(userId,review);
		return Response.noContent().build();
	}
	
	@Override
	public Response getReviewsByBrandId(final Long brandId) {
		BrandReviews reviews = brandService.getReviewsByBrandId( brandId);
		return Response.ok().entity(reviews).build();
	}
	
	

}
