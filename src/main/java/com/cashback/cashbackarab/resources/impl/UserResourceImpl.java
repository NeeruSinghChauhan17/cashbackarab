package com.cashback.cashbackarab.resources.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.cashback.cashbackarab.annotation.Secured;
import com.cashback.cashbackarab.exception.CustomException;
import com.cashback.cashbackarab.models.CashBack;
import com.cashback.cashbackarab.models.ChangePassword;
import com.cashback.cashbackarab.models.Country;
import com.cashback.cashbackarab.models.Login;
import com.cashback.cashbackarab.models.Profile;
import com.cashback.cashbackarab.models.User;
import com.cashback.cashbackarab.models.notification.NotificationStatus;
import com.cashback.cashbackarab.resources.UserResource;
import com.cashback.cashbackarab.service.TransactionService;
import com.cashback.cashbackarab.service.UserService;

@Controller
public class UserResourceImpl implements UserResource {

	final private UserService userService;
	final private TransactionService transactionService;

	@Autowired
	public UserResourceImpl(final UserService userService, final TransactionService transactionService) {
		this.userService = userService;
		this.transactionService = transactionService;
	}

	@Override
	public Response register(final User user, final String userAgent, final String deviceToken,
			final String deviceType, Integer refId) {
		User registerdUser = userService.register(user, userAgent, deviceToken, deviceType,refId);
		transactionService.giveSignupBonus(registerdUser.getUserId());
		if(refId!=null){
			transactionService.giveReferFriendBonus(refId);
		}
		return Response.status(Status.NO_CONTENT).build();
	}

	@Override
	public Response login(final Login login, final String userAgent, final String deviceToken,
			final String deviceType) {
		return Response.ok(userService.login(login, userAgent, deviceToken, deviceType)).build();
	}

	@Override
	public Response socialLogin(final String socialId, final String userAgent, final String deviceToken,
			final String deviceType) {
		return Response.ok(userService.socialLogin(socialId, userAgent, deviceToken, deviceType)).build();
	}

	@Override
	public Response verifyEmail(final Long userId) {
		userService.verifyEmail(userId);
		return Response.ok().entity("Successfully verified").build();
	}

	@Override
	public Response forgotPassword(final String emailId) {
		userService.forgotPassword(emailId);
		Map<String, String> result = new HashMap<String, String>();
		result.put("message", "password send to your register email Id");
		return Response.ok().entity(result).build();
	}

	@Override
	public Response resendVerficationMail(final String emailId) {
		userService.resendVerificationMail(emailId);
		Map<String, String> result = new HashMap<String, String>();
		result.put("message", "verification mail send to your register email Id");
		return Response.ok().entity(result).build();
	}

	@Override
	public Response getAllCountries() {
		List<Country> countries = userService.getAllCountries();
		if (countries.isEmpty()) {
			Response.noContent();
		}
		return Response.ok().entity(countries).build();

	}

	@Override
	public Response getEmailBySocialId(final String socialId) {
		User user = userService.getEmailBySocialId(socialId);
		Map<String, String> result = new HashMap<String, String>();
		result.put("emailId", user.getEmailId());
		return Response.ok().entity(result).build();
	}

	@Secured
	@Override
	public Response getProfileById(final Integer userId) {
		Profile profile = userService.getProfileById(userId);
		return Response.ok().entity(profile).build();

	}

	@Secured
	@Override
	public Response updateUserProfile(final Integer userId, final Profile userProfile) {
		userService.updateUserProfile(userId, userProfile);
		Map<String, String> result = new HashMap<String, String>();
		result.put("message", "profile successfully updated");
		return Response.ok().entity(result).build();

	}

	@Secured
	@Override
	public Response updatePassword(final Integer userId, final ChangePassword changePassword, final String userAgent,
			final String deviceType) {
		if (changePassword.getNewPassword().trim().equals("") || changePassword.getOldPassword().trim().equals("")) {
			throw new CustomException(Status.BAD_REQUEST.getStatusCode(), "password field can not be blank");
		}
		String token = userService.updatePassword(userId, changePassword, userAgent, deviceType);
		Map<String, String> result = new HashMap<String, String>();
		result.put("token", token);
		return Response.ok().entity(result).build();
	}

	@Secured
	@Override
	public Response updateNotificationStatus(final Integer userId, final NotificationStatus status) {
		userService.updateNotificatioStatus(userId, NotificationStatus.getIntValue(status));
		Map<String, String> result = new HashMap<String, String>();
		if (NotificationStatus.getIntValue(status) == 0) {
			result.put("message", "Notification Deactivated");
		} else {
			result.put("message", "Notification Activated");
		}
		return Response.ok().entity(result).build();
	}

	@Secured
	@Override
	public Response updatePushToken(final Integer userId, final String deviceToken, final String deviceType) {
		userService.updatePushToken(userId, deviceToken, deviceType);
		Map<String, String> result = new HashMap<String, String>();
		result.put("message", "push token updated successfully");
		return Response.ok().entity(result).build();
	}

	@Secured
	@Override
	public Response logout(final Integer userId, final String userAgent, final String deviceType) {
		userService.logout(userId, userAgent, deviceType);
		Map<String, String> result = new HashMap<String, String>();
		result.put("message", "user successfully logged out");
		return Response.ok().entity(result).build();
	}

	@Secured
	@Override
	public Response updateProfilePic(final Integer userId, final Map<String, String> pictureUrl) {
		String imageUrl = pictureUrl.get("image_url");
		userService.updateProfilePic(userId, imageUrl);
		Map<String, String> result = new HashMap<String, String>();
		result.put("message", "picture successfully updated");
		return Response.ok().entity(result).build();
	}

	@Secured  
	@Override
	public Response getCashbackHistory(final Integer userId) {
		List<CashBack> cashbackHistory = userService.getCashbackHistory(userId);
		if (cashbackHistory.isEmpty()) {
			Response.noContent();
		}
		return Response.ok().entity(cashbackHistory).build();
	}



	//@Secured   //no need
	@Override
	public Response getSettingValues() {
		Collection<Map> settingValues=userService.getSettingValues();
		return Response.ok().entity(settingValues).build();
	}


	
	

}
