package com.cashback.cashbackarab.resources.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.cashback.cashbackarab.models.notification.Push;
import com.cashback.cashbackarab.resources.NotificationResource;
import com.cashback.cashbackarab.service.NotificationService;

@Controller
public class NotificationResourceImpl implements NotificationResource {

	private final NotificationService ns;

	@Autowired
	NotificationResourceImpl(NotificationService notificationService) {
		this.ns = notificationService;
	}

	@Override
	public Response sendNotification(final Integer userId, final Push push) {
		ns.sendNotification(userId, push);

		return Response.noContent().build();
	}

	@Override
	public Response broadCast(final  Push push) {
		ns.broadCast( push);
		return Response.noContent().build();
	}
	
}
