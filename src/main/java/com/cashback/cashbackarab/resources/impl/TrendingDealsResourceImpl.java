package com.cashback.cashbackarab.resources.impl;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cashback.cashbackarab.models.BrandReviews;
import com.cashback.cashbackarab.models.Review;
import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.resources.TrendingDealsResource;
import com.cashback.cashbackarab.service.TrendingDealsService;

@Component
public class TrendingDealsResourceImpl implements TrendingDealsResource {

	private final TrendingDealsService trendingDealsService;

	@Autowired
	public TrendingDealsResourceImpl(final TrendingDealsService trendingDealsService) {
		this.trendingDealsService = trendingDealsService;
	}

	//@Secured    //no need
	@Override
	public Response getDealsByCategory(final Integer userId,final Integer categoryId) {
		List<Brand> brand = trendingDealsService.getDealsByCategory(userId, categoryId);
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}

	//@Secured          // no need
	@Override
	public Response getDealsByCategoryAndCountry(final Integer userId,final Integer categoryId,final Integer countryId) {
		List<Brand> brand = trendingDealsService.getDealsByCategoryAndCountry(userId, categoryId, countryId);
		if (brand.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(brand).build();
	}
}
