package com.cashback.cashbackarab.resources.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cashback.cashbackarab.annotation.Secured;
import com.cashback.cashbackarab.models.transaction.Payment;
import com.cashback.cashbackarab.models.transaction.RedeemCashback;
import com.cashback.cashbackarab.resources.TransactionResource;
import com.cashback.cashbackarab.service.TransactionService;

@Component
public class TransactionResourceImpl implements TransactionResource {

	private final TransactionService transactionService;

	@Autowired
	public TransactionResourceImpl(final TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	@Secured
	@Override
	public Response getPaymentMethods() {
		List<Payment> paymentMethods = transactionService.getPaymentMethods();
		if (paymentMethods.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok().entity(paymentMethods).build();
	}

	@Secured
	@Override
	public Response redeemCashback(final RedeemCashback redeemCashback, final Integer userId) {
		Double cashbackBalance = transactionService.redeemCashback(redeemCashback, userId);
		Map<String, Double> result = new HashMap<String, Double>();
		result.put("cashback_balance", cashbackBalance);
		return Response.ok().entity(result).build();
	}
	
	@Secured
	@Override
	public Response redeemCashback(final Integer userId) {
		Double cashbackBalance =transactionService.getUserCashbackBalance(userId);
		Map<String, Double> result = new HashMap<String, Double>();
		result.put("cashback_balance", cashbackBalance);
		return Response.ok().entity(result).build();
	}
	
}
