package com.cashback.cashbackarab.resources;



import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cashback.cashbackarab.models.notification.Push;

@Path("cashback/api/v1/notifications")
public interface NotificationResource {
	
	@PUT
	@Path("/users/{user_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response sendNotification(@PathParam("user_id") final Integer userId,final  Push Push);

	
	@PUT
	@Path("/broadcast")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response broadCast( final Push push);
	
	
}
