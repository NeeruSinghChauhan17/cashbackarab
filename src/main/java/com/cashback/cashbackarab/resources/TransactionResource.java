package com.cashback.cashbackarab.resources;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cashback.cashbackarab.models.transaction.RedeemCashback;

@Path("/cashback/api/v1/transaction")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface TransactionResource {

	@GET
	@Path("/user/{user_id}/paymentmethods")
	Response getPaymentMethods();

	@POST
	@Path("/user/{user_id}/redeemcashback")
	Response redeemCashback(@Valid @NotNull final RedeemCashback redeemCashback, @PathParam("user_id") final Integer userId);
	
	@GET
	@Path("/user/{user_id}/cashbackbalance")  
	Response redeemCashback(@Valid @NotNull @PathParam("user_id") final  Integer userId);
	
}
