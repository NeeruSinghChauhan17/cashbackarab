package com.cashback.cashbackarab.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/cashback/api/v1/deal")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface TrendingDealsResource {
	
	@GET
	@Path("/category/{category_id}")  
	Response getDealsByCategory( @DefaultValue("0") Integer userId ,@PathParam("category_id") final Integer categoryId); //no need userId
	
	@GET
	@Path("/category/{category_id}/country/{country_id}")           //no need userId
	Response getDealsByCategoryAndCountry( Integer userId ,@PathParam("category_id") final Integer categoryId, @PathParam("country_id") final Integer countryId); 
	
	
	
}
