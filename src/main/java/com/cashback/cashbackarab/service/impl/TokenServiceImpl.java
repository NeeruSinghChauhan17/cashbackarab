package com.cashback.cashbackarab.service.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cashback.cashbackarab.dao.TokenDao;
import com.cashback.cashbackarab.models.Token;
import com.cashback.cashbackarab.service.TokenService;
import com.google.common.base.Optional;

@Service
public class TokenServiceImpl implements TokenService {

	public static final Logger LOGGER = LoggerFactory.getLogger(TokenServiceImpl.class);

	// private SecureRandom random = new SecureRandom();

	private static final String MD5 = "MD5";

	private final TokenDao tokenDao;

	private final Long tokenTimeToLiveInMinutes;

	@Autowired
	public TokenServiceImpl(final TokenDao tokenDao,
			@Value("${token.time.to.live}") final Long tokenTimeToLiveInMinutes) {
		this.tokenDao = tokenDao;
		this.tokenTimeToLiveInMinutes = tokenTimeToLiveInMinutes;
	}

	@Override
	public String createUserToken(final Integer userId, final String appType, final String deviceId) {
		final Token token = Token.createToken(userId);
		Optional<Token> tokenResp = tokenDao.getUserSession(token, appType, deviceId);
		if (tokenResp.isPresent()) {
			tokenDao.updateUserToken(token, appType, deviceId);
		} else {
			tokenDao.saveUserToken(token, appType, deviceId);
		}

		return generateToken(token.getUniqueId() + token.getUserId());
	}

	@Override
	public boolean verifyUserToken(final Integer userId, final String tokenId, final String appType,final String deviceId) {

		LOGGER.info("The token for user={} is {} and deviceId is {}", userId, tokenId,deviceId);

		final Token token = tokenDao.getUserToken(userId, appType, deviceId);
		final String dbTokenId = generateToken(token.getUniqueId() + token.getUserId());
		LOGGER.info(" =================The  dbTokenId for user={} is {} and deviceId is {} ===============", userId, dbTokenId,deviceId);
		if (tokenId.equalsIgnoreCase(dbTokenId)) {
			// This is the case when a token never expires
			if (tokenTimeToLiveInMinutes == 0) {
				LOGGER.info("The token for user={} is {} matched", userId, tokenId);
				return true;
			}
			final Duration duration = new Duration(token.getCreationDateTime(), DateTime.now());
			if (tokenTimeToLiveInMinutes.compareTo(duration.getStandardMinutes()) < 0) {
				LOGGER.info("The token for user={} is {} matched but expired", userId, tokenId);
				return false;
			}
			return true;
		}
		LOGGER.info("The token for user={} is {} not matched", userId, tokenId);
		return false;
	}

	private String generateToken(final String randomString) {
		try {
			final MessageDigest messageDigest = MessageDigest.getInstance(MD5);
			final byte[] array = messageDigest.digest(randomString.getBytes());
			final StringBuilder builder = new StringBuilder();
			for (int i = 0; i < array.length; ++i) {
				builder.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return builder.toString();

		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException("Something went wrong while creating token");
		}
	}

	@Override
	public boolean isUserExistWithApp(final Integer userId,final String appType) {
		Collection<Token> tokenResp = tokenDao.isUserTokenExistWithApp(userId, appType);
		if (!tokenResp.isEmpty() || tokenResp.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void deleteAllTokenByUserId(final Integer userId) {
		tokenDao.deleteAllTokenByUserId(userId);
	}

	@Override
	public void invalidateToken(final Integer userId,final  String appType, final String deviceId) {
		tokenDao.invalidateToken(userId, appType, deviceId);
	}

}
