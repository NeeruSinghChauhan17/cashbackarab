package com.cashback.cashbackarab.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cashback.cashbackarab.dao.UserDao;
import com.cashback.cashbackarab.exception.ConflictException;
import com.cashback.cashbackarab.exception.ForbiddenException;
import com.cashback.cashbackarab.exception.NotFoundException;
import com.cashback.cashbackarab.exception.UnAuthorizedException;
import com.cashback.cashbackarab.mail.EmailServiceImpl;
import com.cashback.cashbackarab.models.CashBack;
import com.cashback.cashbackarab.models.ChangePassword;
import com.cashback.cashbackarab.models.Country;
import com.cashback.cashbackarab.models.Login;
import com.cashback.cashbackarab.models.Profile;
import com.cashback.cashbackarab.models.User;
import com.cashback.cashbackarab.models.notification.NotificationStatus;
import com.cashback.cashbackarab.service.TokenService;
import com.cashback.cashbackarab.service.TransactionService;
import com.cashback.cashbackarab.service.UserService;
import com.cashback.cashbackarab.utils.RandomString;
import com.cashback.cashbackarab.utils.aes.MD5util;
import com.google.common.base.Optional;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	private final UserDao userDao;
	private final EmailServiceImpl emailService;
	private final ThreadPoolTaskExecutor taskExecutor;
	private final TokenService tokenService;
	private final TransactionService transactionService;

	@org.springframework.beans.factory.annotation.Value("${${mode}.storage.p12.path}")
	private String storageFilePath;

	@org.springframework.beans.factory.annotation.Value("${${mode}.email.verification.url}")
	private String emailVerificationUrl;

	@Autowired
	public UserServiceImpl(final UserDao userDao, final EmailServiceImpl emailService,
			final ThreadPoolTaskExecutor taskExecutor, final TokenService tokenService,
			final TransactionService transactionService) {
		this.userDao = userDao;
		this.emailService = emailService;
		this.taskExecutor = taskExecutor;
		this.tokenService = tokenService;
		this.transactionService = transactionService;
	}

	@Override
	@Transactional
	public User register(final User user, final String userAgent, final String deviceToken, final String deviceType,
			final Integer refId) {
		if (user.getSocialId() != null) {
			final Optional<User> socialusser = userDao.isSocialIdExist(user.getSocialId());
			if (socialusser.isPresent()) {
				throw new ConflictException("SocialId already exist");
			}
		}
		final Optional<User> usser = userDao.isExists(user.getEmailId());

		if (usser.isPresent() && usser.get().getPassword() != null) {
			LOGGER.error("user exist with " + usser.get().getEmailId() + " " + usser.get().getUserId());
			throw new ConflictException("user already exist with email");
		} else if (usser.isPresent() && usser.get().getPassword() == null) {
			userDao.updateUser(usser.get().getUserId(), user, false);
	    	return usser.get();
		}
		final User usr = saveUser(user, refId);
		taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				sendEmail(usr.getUserId(), usr.getEmailId(), usr.getFname());

			}
		});
		return usr;
	}

	@Override
	public User login(final Login login, final String userAgent, final String deviceToken, final String deviceType) {
		final Optional<User> usr = userDao.getUserDetails(login);
		if (!usr.isPresent()) {
			throw new NotFoundException("user not exist");
		}
		if (usr.isPresent() && usr.get().getPassword().equals(RandomString.encryptPassword(login.getPassword()))) {

			if (usr.get().getStatus().equals("inactive")) {
				throw new ForbiddenException("Email not verified");
			}

			userDao.updatePushToken(usr.get().getUserId(), deviceToken, deviceType);
			String token = tokenService.createUserToken(usr.get().getUserId(), deviceType, userAgent);
			Double cashbackBalance = transactionService.getUserCashbackBalance(usr.get().getUserId());
			return User.createWithTokenWithNoPassword(token, usr.get(), cashbackBalance);
		} else {
			LOGGER.error("User Not found after login.");
		}
		throw new UnAuthorizedException("wrong emailid or password");
	}

	@Override
	public User socialLogin(final String socialId, final String userAgent, final String deviceToken,
			final String deviceType) {
		final Optional<User> usr = userDao.getUserDetailsbySocialId(socialId);
		if (usr.isPresent()) {
			if (usr.get().getStatus().equals("inactive")) {
				throw new ForbiddenException("Email not verified");
			}

			userDao.updatePushToken(usr.get().getUserId(), deviceToken, deviceType);

			String token = tokenService.createUserToken(usr.get().getUserId(), deviceType, userAgent);
			Double cashbackBalance = transactionService.getUserCashbackBalance(usr.get().getUserId());
			return User.createWithTokenWithNoPassword(token, usr.get(), cashbackBalance);
		} else {
			LOGGER.error("User Not found after login.");
		}
		throw new NotFoundException("user not exist");
	}

	private void sendEmail(final Integer userId, final String emailId,final String fname) {
		String url = emailVerificationUrl + userId + "/verify";
		emailService.sendWelcomeMail(emailId, url, fname);
	}

	@Transactional
	public User saveUser(final User user, final Integer refId) {
		final User usr = userDao.save(user, refId);
		return usr;
	}

	@Override
	public void verifyEmail(final Long userId) throws NotFoundException {
		userDao.verifyEmail(userId);

	}

	@Override
	public void forgotPassword(final String emailId) {
		final Optional<User> user = userDao.isExists(emailId);
		if (user.isPresent() && user.get().getEmailId() != null) {
			String fname = user.get().getFname();
			final String password = RandomString.generateRandomString();
			userDao.updatePassword(emailId, RandomString.encryptPassword(password));
			taskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					emailService.sendForgotPasswordMail(emailId, password, fname);

				}
			});
		}
	}

	@Override
	public void resendVerificationMail(final String emailId) {
		final Optional<User> user = userDao.isExists(emailId);
		if (user.isPresent() && user.get().getEmailId() != null) {
			taskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					sendEmail(user.get().getUserId(), user.get().getEmailId(), user.get().getFname());

				}
			});
		}
	}

	@Override
	public List<Country> getAllCountries() {
		return userDao.getAllCountries();
	}

	@Override
	public User getEmailBySocialId(final String socialId) {
		final Optional<User> usr = userDao.getUserDetailsbySocialId(socialId);
		if (usr.isPresent()) {
			return usr.get();
      
		} else {
			LOGGER.error("User Not found after login.");
		}
		throw new NotFoundException("user not exist");
	}

	@Override
	public Profile getProfileById(final Integer userId) {
		final Optional<Profile> userProfile = userDao.getProfileById(userId);
		if (userProfile.isPresent()) {
			Double cashbackBalance = transactionService.getUserCashbackBalance(userId);
			return Profile.profileWithCashbackBalances(userId, userProfile.get(), cashbackBalance);
		}
		throw new NotFoundException("user not exist");
	}

	@Override
	public Profile updateUserProfile(final Integer userId,final Profile userProfile) {
		final Optional<Profile> user = userDao.getProfileById(userId);
		if (user.isPresent()) {
			userDao.updateuserProfile(userId, userProfile);
			return userProfile;
		}
		throw new NotFoundException("user not exist");
	}

	@Override
	public String updatePassword(final Integer userId, final ChangePassword changePassword, final String userAgent,
			final String deviceType) {
		final Optional<Profile> user = userDao.getProfileById(userId);
		if (!user.isPresent()) {
			throw new NotFoundException("user not exist");
		}
		if (user.isPresent() && user.get().getPassword()
				.equals(RandomString.encryptPassword(changePassword.getOldPassword())) == false) {
			throw new UnAuthorizedException("old password not matched");
		}
		userDao.changePassword(userId, changePassword);

		tokenService.deleteAllTokenByUserId(userId);

		String token = tokenService.createUserToken(userId, deviceType, userAgent);

		return token;
	}

	public String createToken(final String baseInfo) {
		if (baseInfo != null) {
			String token = MD5util.getMD5(baseInfo);
			return token;
		}
		return null;
	}

	@Override
	public void updateNotificatioStatus(final Integer userId,final Integer status) {
		userDao.updateNotificationStatus(userId, status);
	}

	@Override
	public void updatePushToken(final Integer userId,final  String deviceToken,final String deviceType) {
		userDao.updatePushToken(userId, deviceToken, deviceType);
	}

	@Override
	public void logout(final Integer userId,final  String userAgent,final String deviceType) {
		final Optional<Profile> user = userDao.getProfileById(userId);
		if (!user.isPresent()) {
			throw new NotFoundException("user not exist");
		}
		tokenService.invalidateToken(userId, deviceType, userAgent);
		userDao.updateNotificationStatus(userId, NotificationStatus.getIntValue(NotificationStatus.DEACTIVE));
	}

	@Override
	public void updateProfilePic(final Integer userId,final String imageUrl) {
		userDao.updateProfilePic(userId, imageUrl);

	}

	@Override
	public List<CashBack> getCashbackHistory(final Integer userId) {
		return userDao.getCashbackHistory(userId);
	}

	

	@Override
	public Collection<Map> getSettingValues() {

		return userDao.getSettingValues();
	}

}
