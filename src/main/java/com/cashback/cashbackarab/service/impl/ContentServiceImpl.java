package com.cashback.cashbackarab.service.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cashback.cashbackarab.content.CashbackApp;
import com.cashback.cashbackarab.content.FirebaseCustomToken;
import com.cashback.cashbackarab.service.ContentService;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.tasks.OnSuccessListener;
import com.google.firebase.tasks.Task;

@Service
public class ContentServiceImpl implements ContentService {

	private static final String CASHBACK_BUCKET_URL = "https://cashbackarab-f03a0.appspot.com/";
	private static final String CONTENT_CLASS_PATH = "/content/";
	private static final String CASHBACK_BUCKET_FILE = "cashbackarab_firebase.json";
	private static final Logger LOGGER = LoggerFactory.getLogger(ContentServiceImpl.class);

	// app options
	private Boolean isCachbackAppInitialize = Boolean.FALSE;

	@Override
	public Boolean initializeFirebaseApp(final CashbackApp app) {
		try {
			final InputStream weDoLogisticServiceAccount = this.getClass().getResourceAsStream(app.getBucketFilePath());

			final FirebaseOptions firebaseOptions = new FirebaseOptions.Builder()
					.setCredential(FirebaseCredentials.fromCertificate(weDoLogisticServiceAccount))
					.setDatabaseUrl(app.getBucketUrl()).build();

			FirebaseApp.initializeApp(firebaseOptions, app.getAppName());
		} catch (Exception e) {
			LOGGER.error("Error while initializing firebase app: {}", app.getAppName(), e);
			return Boolean.FALSE;
		}
		LOGGER.info("Initialized firebase app: {}", app.getAppName());
		return Boolean.TRUE;
	}

	private void init() {

		if (!isCachbackAppInitialize) {
			isCachbackAppInitialize = initializeFirebaseApp(
					new CashbackApp("Cashbackarab", CONTENT_CLASS_PATH + CASHBACK_BUCKET_FILE, CASHBACK_BUCKET_URL));
		}

	}

	@Override
	public Optional<FirebaseCustomToken> getFirebaseCustomToken(final Integer userId) {
		init();
		final FirebaseApp app = FirebaseApp.getInstance("Cashbackarab");
		final CountDownLatch latch = new CountDownLatch(1);
		final HashMap<String, Object> additionalClaims = new HashMap<String, Object>();
		additionalClaims.put("premiumAccount", true);

		final Task<String> tasks = FirebaseAuth.getInstance(app)
				.createCustomToken(String.valueOf(userId), additionalClaims)
				.addOnSuccessListener(new OnSuccessListener<String>() {
					@Override
					public void onSuccess(String customToken) {
						latch.countDown();
					}
				});
		try {
			latch.await();
		} catch (InterruptedException e) {
			LOGGER.error("Error while create custom token", e);
			return Optional.empty();
		}
		return Optional.ofNullable(new FirebaseCustomToken(tasks.getResult()));
	}

}
