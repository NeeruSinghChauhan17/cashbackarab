package com.cashback.cashbackarab.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cashback.cashbackarab.dao.TransactionDao;
import com.cashback.cashbackarab.exception.BadRequestException;
import com.cashback.cashbackarab.models.http.Result;
import com.cashback.cashbackarab.models.transaction.Payment;
import com.cashback.cashbackarab.models.transaction.RedeemCashback;
import com.cashback.cashbackarab.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

	private TransactionDao transactionDao;

	@Autowired
	public TransactionServiceImpl(final TransactionDao transactionDao) {
		this.transactionDao = transactionDao;
	}

	@Override
	public void giveSignupBonus(final Integer userId) {

		transactionDao.giveSignupBonus(userId);
	}

	@Override
	public Double getUserCashbackBalance(final Integer userId) {

		return transactionDao.getUserCashbackBalance(userId);

	}
      
	@Override
	public List<Payment> getPaymentMethods() {
		return transactionDao.getPaymentMethods();
	}
     
	@Override
	public Double redeemCashback(final RedeemCashback redeemCashback, final Integer userId) {
		Double cashBackBalance = transactionDao.getUserCashbackBalance(userId);
		if (cashBackBalance < redeemCashback.getAmount()) {
			throw new BadRequestException("Insufficient balance");
		}
		transactionDao.redeemCashback(redeemCashback, userId);
		return cashBackBalance - redeemCashback.getAmount();
	}

	@Override
	public void giveReferFriendBonus(final Integer refId) {
		transactionDao.giveReferFriendBonus(refId);

	}

	@Override
	public void giveCashback(final List<Result> results) {
		transactionDao.giveCashback(results);

	}

}
