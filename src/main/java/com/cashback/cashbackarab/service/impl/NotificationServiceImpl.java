package com.cashback.cashbackarab.service.impl;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.cashback.cashbackarab.dao.UserDao;
import com.cashback.cashbackarab.models.DeviceInfo;
import com.cashback.cashbackarab.models.User;
import com.cashback.cashbackarab.models.notification.Push;
import com.cashback.cashbackarab.notification.PushNotifier;
import com.cashback.cashbackarab.service.NotificationService;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

@Service
public class NotificationServiceImpl implements NotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);
	private final UserDao userDao;
	private  final Integer OFFSET=0;
	private final Integer LIMIT=30;
	private final Map<String, String> userTypeSenderIds;
	private final PushNotifier pushNotifier;

	@Autowired
	NotificationServiceImpl(final UserDao userDao, final ThreadPoolTaskExecutor bgExecutor,
			final PushNotifier pushNotifier,
			@Value("#{userTypeSenderIds}") final Map<String, String> userTypeSenderIds) {
		this.userDao = userDao;
		this.userTypeSenderIds = userTypeSenderIds;
		this.pushNotifier = pushNotifier;
	}

	@Override
	public void sendNotification(final Integer userId, final Push push) {
		
		final Optional<User> usrOp = findUserById(userId);

		if (usrOp.isPresent()) {

			final Optional<DeviceInfo> di = getDeviceInfo(userId);
			if (di.isPresent() && !StringUtils.isEmpty(di.get().getDeviceToken())) {

				final Optional<Long> senderIdOp = getSenderId("cashback");
				if (senderIdOp.isPresent()) {
					pushNotifier.sendPush(
							Push.createWithSenderIdAndDevices(senderIdOp.get(), Lists.newArrayList(di.get()), push));
				} else {
					LOGGER.info("Push SenderId not found for user type = {} unable to send Push notification.");
				}
			} else {
				LOGGER.info("Device token not found for userId = {} unable to send Push notification.", userId);
			}
		} else {
			LOGGER.info("User Or UserType not found for userId = {} unable to send Push notification.", userId);
		}
	}

	private Optional<DeviceInfo> getDeviceInfo(final Integer userId) {
		return userDao.findLastActiveDeviceInfoById(userId);
	}

	private Optional<Long> getSenderId(final String project) {
		if (userTypeSenderIds.containsKey(project)) {
			return Optional.fromNullable(Long.valueOf(userTypeSenderIds.get(project)));
		}
		return Optional.<Long>absent();
	}

	private Optional<User> findUserById(final Integer userId) {
		return userDao.findUserById(userId);
	}

	
	private List<DeviceInfo> getDevicesInfo(final Integer offset,final Integer limit){
		return userDao.getDevicesInfo(offset,limit);
	}

	@Override
	public void broadCast(final  Push push) {
		sendNotifications(push,OFFSET,LIMIT);
		}
	
	private void sendNotifications(final  Push push,final Integer offset,final Integer limit){
		List<DeviceInfo> di=getDevicesInfo(offset,limit);
		if(di.isEmpty()){ 
		return; }
		final Optional<Long> senderIdOp = getSenderId("cashback");
		 if (senderIdOp.isPresent()) {
			pushNotifier.sendPush(Push.createWithSenderIdAndDevices(senderIdOp.get(), di, push));
			  LOGGER.info("broadCasting on total="+di.size() +" devices");
		} else {
			LOGGER.error("Push SenderId not found for user type = {} unable to send Push notification.");
		}
		sendNotifications(push,offset+limit,limit);
	}
	

}
