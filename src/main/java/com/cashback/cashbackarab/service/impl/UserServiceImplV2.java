package com.cashback.cashbackarab.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cashback.cashbackarab.dao.UserDaoV2;
import com.cashback.cashbackarab.exception.AccountLockedException;
import com.cashback.cashbackarab.exception.ConflictException;
import com.cashback.cashbackarab.exception.ErrorEntity;
import com.cashback.cashbackarab.exception.ForbiddenException;
import com.cashback.cashbackarab.exception.NotFoundException;
import com.cashback.cashbackarab.exception.UnAuthorizedException;
import com.cashback.cashbackarab.mail.EmailServiceImpl;
import com.cashback.cashbackarab.models.CashBack;
import com.cashback.cashbackarab.models.ChangePassword;
import com.cashback.cashbackarab.models.Country;
import com.cashback.cashbackarab.models.InternationalNumber;
import com.cashback.cashbackarab.models.Login;
import com.cashback.cashbackarab.models.Profile;
import com.cashback.cashbackarab.models.UpdatePassword;
import com.cashback.cashbackarab.models.User;
import com.cashback.cashbackarab.models.notification.NotificationStatus;
import com.cashback.cashbackarab.service.TokenService;
import com.cashback.cashbackarab.service.TransactionService;
import com.cashback.cashbackarab.service.UserServiceV2;
import com.cashback.cashbackarab.utils.EncryptDecrypt;
import com.cashback.cashbackarab.utils.RandomString;
import com.cashback.cashbackarab.utils.aes.MD5util;
import com.google.common.base.Optional;

@Service
public class UserServiceImplV2 implements UserServiceV2 {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImplV2.class);

	private static final String NULL = null;

	private final UserDaoV2 userDao;
	private final EmailServiceImpl emailService;
	private final ThreadPoolTaskExecutor taskExecutor;
	private final TokenService tokenService;
	private final TransactionService transactionService;
	private final String INACTIVE="inactive";

	@org.springframework.beans.factory.annotation.Value("${${mode}.storage.p12.path}")
	private String storageFilePath;

	@org.springframework.beans.factory.annotation.Value("${${mode}.email.verification.url}")
	private String emailVerificationUrl;

	@Autowired
	public UserServiceImplV2(final UserDaoV2 userDao, final EmailServiceImpl emailService,
			final ThreadPoolTaskExecutor taskExecutor, final TokenService tokenService,
			final TransactionService transactionService) {
		this.userDao = userDao;
		this.emailService = emailService;
		this.taskExecutor = taskExecutor;
		this.tokenService = tokenService;
		this.transactionService = transactionService;
	}

	@Override
	@Transactional
	public User register(final User user, final String userAgent, final String deviceToken, final String deviceType,
			final Integer refId) {
		Optional<User> usser = null;
		if (user.getSocialId() != null) {
			final Optional<User> socialusser = userDao.isSocialIdExist(user.getSocialId()); //check is user already exist with social-account
			 if (socialusser.isPresent()) {
				throw new ConflictException("SocialId already exist");
			}
		}

	/*  if (user.getEmailId().contains("@")) {
			usser = userDao.isUserExistsWithEmail(user.getEmailId());
			if (usser.isPresent()) {
				LOGGER.error("user already exist(email) with " + usser.get().getUserId());
				throw new ConflictException("email already exist");
			}
		}*/
		if ((user.getNumber().getCc()!=null)&& (user.getNumber().getCc()!="") && (user.getNumber().getNumber().trim() != "") 
				&& user.getNumber().getNumber()!=null   && (user.getNumber().getCode()!="") && (user.getNumber().getNumber()!=null)) {
            usser = userDao.isUserExistsWithNo(user.getNumber());
			if (usser.isPresent()) {
				LOGGER.error("user already exist(phone no) with " + usser.get().getUserId());
				throw new ConflictException("phone no already exist");
			}
		}

		if (usser.isPresent() && usser.get().getPassword() == null) {
			userDao.updateUser(usser.get().getUserId(), user, false);
			return usser.get();
		}

		final User usr = saveUser(user, refId);
		return usr;
	}

	@Override
	public User login(final Login login, final String userAgent, final String deviceToken, final String deviceType) throws AccountLockedException {
	
		final Optional<User> usr = getUserDetails(login);
        if (!usr.isPresent()) {
			throw new NotFoundException("user not exist");
		}
       
        if (userDao.isSocialUserWithUserId(usr.get().getUserId())) {
				throw new ConflictException( "This no is already registered with social id");
        } 
        
        /**
         * no need to check status(status=active serves same purpose of email_verified in V1,but in v2 by phone_number_verified)
         */
        
     /*   if (usr.get().getStatus().equals(INACTIVE)) {
			throw new AccountLockedException.Builder().entity(new ErrorEntity(2,"account is blocked/locked")).build();
		}*/
        
        if ( !(usr.get().getPassword().equals(RandomString.encryptPassword(login.getPassword())))) {
        	throw new UnAuthorizedException("incorrect password");
        }
        

		userDao.updatePushToken(usr.get().getUserId(), deviceToken, deviceType);
		String token = tokenService.createUserToken(usr.get().getUserId(), deviceType, userAgent);
		Double cashbackBalance = transactionService.getUserCashbackBalance(usr.get().getUserId());
		if((usr.get().getIsNoVerified().equals("false"))){
		 return User.createWithTokenWithNoPasswordandWithNoNumberV2(token, usr.get(), cashbackBalance);	
		}
		return User.createWithTokenWithNoPasswordV2(token, usr.get(), cashbackBalance);
        
  	}

	private Optional<User> getUserDetails(Login login) {
		Optional<User> user = null;
      
		  if ((login.getEmailId() != NULL) && (login.getEmailId() !="") && (login.getEmailId() !=" ") && (login.getEmailId().contains("@"))) {
			user = userDao.getUserDetailsByEmail(login);
			  if(user.isPresent() && user.get().getIsEmailVerified()==0){
				  throw new ForbiddenException("Email not verified"); 
			  }
            return user;
		  } else 
			{
			  if ((login.getNumber().getNumber()!= NULL) && (login.getNumber().getNumber() != "") && (login.getNumber().getCc()!=null) && 
				(login.getNumber().getCc()!="") && (login.getNumber().getCode()!=null) && (login.getNumber().getCode()!="")) {
                  user = userDao.getUserDetailsByPhone(login.getNumber());
                   
			 }
            return user;
	      }
    
	}

	@Override
	public User socialLogin(final String socialId, final String userAgent, final String deviceToken,
			final String deviceType) {
		final Optional<User> usr = userDao.getUserDetailsbySocialId(socialId);
		if (usr.isPresent()) {
			if (usr.get().getStatus().equals("inactive")) {
				throw new ForbiddenException("Email not verified");
			}

			userDao.updatePushToken(usr.get().getUserId(), deviceToken, deviceType);

			String token = tokenService.createUserToken(usr.get().getUserId(), deviceType, userAgent);
			Double cashbackBalance = transactionService.getUserCashbackBalance(usr.get().getUserId());
			return User.createWithTokenWithNoPassword(token, usr.get(), cashbackBalance);
		} else {
			LOGGER.error("User Not found after login.");
		}
		throw new NotFoundException("user not exist");
	}

	private void sendEmail(final Integer userId, final String emailId, final String fname) {
		String url = emailVerificationUrl + userId + "/verify";
		emailService.sendWelcomeMail(emailId, url, fname);
	}

	@Transactional
	public User saveUser(final User user, final Integer refId) {
		final User usr = userDao.save(user, refId);
		return usr;
	}

	@Override
	public void verifyEmail(final Long userId) throws NotFoundException {
		userDao.verifyEmail(userId);

	}

	@Override
	public void forgotPassword(final String emailId) {
		final Optional<User> user = userDao.isUserExistsWithEmail(emailId);
		if (user.isPresent() && user.get().getEmailId() != null) {
			String fname = user.get().getFname();
			final String password = RandomString.generateRandomString();
			userDao.updatePassword(emailId, RandomString.encryptPassword(password));
			taskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					emailService.sendForgotPasswordMail(emailId, password, fname);

				}
			});
		}
	}

	@Override
	public void resendVerificationMail(final String emailId) {
		final Optional<User> user = userDao.isUserExistsWithEmail(emailId);
		if (user.isPresent() && user.get().getEmailId() != null) {
			taskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					sendEmail(user.get().getUserId(), user.get().getEmailId(), user.get().getFname());

				}
			});
		}
	}

	@Override
	public List<Country> getAllCountries() {
		return userDao.getAllCountries();
	}

	@Override
	public User getEmailBySocialId(final String socialId) {
		final Optional<User> usr = userDao.getUserDetailsbySocialId(socialId);
		if (usr.isPresent()) {
			return usr.get();

		} else {
			LOGGER.error("User Not found after login.");
		}
		throw new NotFoundException("user not exist");
	}

	@Override
	public Profile getProfileById(final Integer userId) {
		final Optional<Profile> userProfile = userDao.getProfileById(userId);
		if (userProfile.isPresent()) {
			Double cashbackBalance = transactionService.getUserCashbackBalance(userId);
			return Profile.profileWithCashbackBalancesV2(userId, userProfile.get(), cashbackBalance);
		}
		throw new NotFoundException("user not exist");
	}

	@Override
	public Profile updateUserProfile(final Integer userId, final Profile userProfile) {
		final Optional<Profile> user = userDao.getProfileById(userId);
		if (user.isPresent()) {
			userDao.updateuserProfile(userId, userProfile);
			return userProfile;
		}
		throw new NotFoundException("user not exist");
	}

	@Override
	public String updatePassword(final Integer userId, final ChangePassword changePassword, final String userAgent,
			final String deviceType) {
		final Optional<Profile> user = userDao.getProfileById(userId);
		if (!user.isPresent()) {
			throw new NotFoundException("user not exist");
		}
		if (user.isPresent() && user.get().getPassword()
				.equals(RandomString.encryptPassword(changePassword.getOldPassword())) == false) {
			throw new UnAuthorizedException("old password not matched");
		}
		userDao.changePassword(userId, changePassword);

		tokenService.deleteAllTokenByUserId(userId);

		String token = tokenService.createUserToken(userId, deviceType, userAgent);

		return token;
	}

	public String createToken(final String baseInfo) {
		if (baseInfo != null) {
			String token = MD5util.getMD5(baseInfo);
			return token;
		}
		return null;
	}

	@Override
	public void updateNotificatioStatus(final Integer userId, final Integer status) {
		userDao.updateNotificationStatus(userId, status);
	}

	@Override
	public void updatePushToken(final Integer userId, final String deviceToken, final String deviceType) {
		userDao.updatePushToken(userId, deviceToken, deviceType);
	}

	@Override
	public void logout(final Integer userId, final String userAgent, final String deviceType) {
		final Optional<Profile> user = userDao.getProfileById(userId);
		if (!user.isPresent()) {
			throw new NotFoundException("user not exist");
		}
		tokenService.invalidateToken(userId, deviceType, userAgent);
		userDao.updateNotificationStatus(userId, NotificationStatus.getIntValue(NotificationStatus.DEACTIVE));
	}

	@Override
	public void updateProfilePic(final Integer userId, final String imageUrl) {
		userDao.updateProfilePic(userId, imageUrl);

	}

	@Override
	public List<CashBack> getCashbackHistory(final Integer userId) {
		return userDao.getCashbackHistory(userId);
	}

	@Override
	public Collection<Map> getSettingValues() {

		return userDao.getSettingValues();
	}

	@Override
	public void verifyPhoneNumber(InternationalNumber number) {
		if (!userDao.phoneNumberVerified(number)) {
			throw new NotFoundException("user not exist");
		}

	}

	@Override
	public void forgetUpdatePassword(UpdatePassword password) {
		String encrStr = EncryptDecrypt.getEncryptedString(password.getCode(), password.getPassword());
		LOGGER.info("================ ENCRYTED STRING  =====================" + encrStr);
		if (userDao.isNoExists(password.getNumber())) { // true-if no exists
			if (encrStr.equals(password.getEncryptString())) {
				userDao.forgetUpdatePassword(password);
			} else {
				LOGGER.error("================ ENCRYTED STRING DOES NOT MATCHED =====================" + encrStr);
				throw new UnAuthorizedException("code not matched");
			}
		} else {
			LOGGER.error("================ USER NOT EXISTS WITH THIS PHONE NO=====================");
			throw new NotFoundException(" user not exists with this no ");
		}

	}

	@Override
	public boolean isNoExists(final InternationalNumber number) {
		if (userDao.isNoExists(number)) {
			return true;
		}
		return false;
	}

	@Override
	public void updateNumber(Integer userId,InternationalNumber number) {
		if(userDao.isNoExists(number)){
		  userDao.deleteNumberFromOldUser(number); 
		}
	   userDao.updateNumber(userId,number);
		
	}

}
