package com.cashback.cashbackarab.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cashback.cashbackarab.dao.TrendingDealsDao;
import com.cashback.cashbackarab.models.BrandReviews;
import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.service.TrendingDealsService;

@Service
public class TrendingDealsServiceImpl implements TrendingDealsService {

	private final TrendingDealsDao trendingDealsDao;

	@Autowired
	public TrendingDealsServiceImpl(final TrendingDealsDao trendingDealsDao) {
		this.trendingDealsDao = trendingDealsDao;
	}

	@Override
	public List<Brand> getDealsByCategory(final Integer userId, final Integer categoryId) {
		return trendingDealsDao.getDealsByCategory(userId, categoryId);
	}

	@Override
	public List<Brand> getDealsByCategoryAndCountry(final Integer userId, final Integer categoryId,final Integer countryId) {
		return trendingDealsDao.getDealsByCategoryAndCountry(userId, categoryId, countryId);
	}

	

}
