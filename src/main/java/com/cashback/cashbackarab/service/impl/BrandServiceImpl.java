package com.cashback.cashbackarab.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cashback.cashbackarab.dao.BrandDao;
import com.cashback.cashbackarab.exception.BadRequestException;
import com.cashback.cashbackarab.models.Action;
import com.cashback.cashbackarab.models.Banner;
import com.cashback.cashbackarab.models.BrandReviews;
import com.cashback.cashbackarab.models.Review;
import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.models.brand.Coupon;
import com.cashback.cashbackarab.models.brand.ShoppingTrips;
import com.cashback.cashbackarab.service.BrandService;


@Service
public class BrandServiceImpl implements BrandService {

	// private static final Logger LOGGER =
	// LoggerFactory.getLogger(BrandServiceImpl.class);

	private BrandDao brandDao;

	@Autowired
	public BrandServiceImpl(final BrandDao brandDao) {
		this.brandDao = brandDao;
	}

	@Override
	public List<Brand> getTrendingDetails(final Integer userId, final Integer countryId) {
		return brandDao.getTrendingDeals(userId, countryId);
	}

	@Override
	public List<Brand> getAllTrendingDetails() {
		
		return brandDao.getAllTrendingDeals();
	}

	@Override
	public List<Brand> getTrendingStores(final Integer userId, final Integer countryId) {
		return brandDao.getTrendingStores(userId, countryId);
	}

	@Override
	public List<Brand> getAllTrendingStores(final Integer userId) {
		
		return brandDao.getAllTrendingStores(userId);
		
		
	}

	@Override
	public List<Brand> getSameCategoryBrands(final Integer userId,final Integer brandId) {
		return brandDao.getSameCategoryBrands(userId, brandId);
	}

	@Override
	public List<Coupon> getCouponsByBrandId(final Integer userId,final  Integer brandId) {
		return brandDao.getCouponsByBrandId(userId, brandId);
	}

	@Override
	public List<Brand> getCoupons() {
		return brandDao.getCoupons();
	}

	@Override
	public List<Brand> getCouponsByCountry(final Integer countryId) {
		return brandDao.getCouponsByCountry(countryId);
	}

	@Override
	public List<Brand> getCouponsByCategory(final Integer caregoryId) {
		return brandDao.getCouponsByCategory(caregoryId);
	}

	@Override
	public List<Brand> getCouponsByCategoryAndCountry(final Integer caregoryId,final Integer countryId) {
		return brandDao.getCouponsByCategoryAndCountry(caregoryId, countryId);
	}

	/*
	 * @Override public List<Brand> getTrendingOfferImages(Integer countryId) {
	 * return }
	 */

	@Override
	public List<Brand> getFeaturedDeals() {
		return brandDao.getFeaturedDeals();
	}

	@Override
	public void activateDeal(final Integer userId, final Integer brandId, final Integer couponId) {
		brandDao.activateDeal(userId, brandId, couponId);
	}

	@Override
	public Optional<Brand> getBrandDeatils(final Integer userId, final Integer brandId) {
		
		return brandDao.getBrandDetails(userId, brandId);
		
		
	}

	@Override
	public Map<String, Object> getSearchDetails(final Integer userId, String brandName) {
		// TODO Auto-generated method stub
		return brandDao.getsearchDetails(userId, brandName);
	}

	@Override
	public void addOrRemoveStoreFromFavourite(final Integer brandId, final Integer userId, final Action action) {

		brandDao.addOrRemoveStoreFromFavourite(brandId, userId, action);
	}

	@Override
	public List<Brand> getFavouriteBrands(final Integer userId) {

		return brandDao.getFavouriteBrands(userId);
	}

	@Override
	public List<ShoppingTrips> getShoppingTrips(final Integer userId) {
		return brandDao.getShoppingTrips(userId);
	}

	@Override
	public List<Banner> getBannerImages() {
		return brandDao.getBannerImages();
	}
	@Override
	public void addReview(final Integer userId,final  Review review) {
		
		if (review.getRating() > 5 || review.getRating() < 0 || review.getRating().equals(null)) {
			throw new BadRequestException("Rating range should be 1-5");
		}
		brandDao.deleteExistingReviewifExist(userId, review);
		brandDao.addReview(userId, review);

	}

	@Override
	public BrandReviews getReviewsByBrandId(final Long brandId) {
		return BrandReviews.withReviewAndAverageRating( brandDao.getReviewsAverageRating( brandId).get(),brandDao.getReviewsByBrandId( brandId));
	}
}
