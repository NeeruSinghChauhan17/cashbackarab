package com.cashback.cashbackarab.service;

import java.util.List;

import com.cashback.cashbackarab.models.http.Result;
import com.cashback.cashbackarab.models.transaction.Payment;
import com.cashback.cashbackarab.models.transaction.RedeemCashback;

public interface TransactionService {

	void giveSignupBonus(final Integer userId);

	Double getUserCashbackBalance(final Integer userId);

	List<Payment> getPaymentMethods();

	Double redeemCashback(final RedeemCashback redeemCashback, final Integer userId);

	void giveReferFriendBonus(final Integer refId);

	void giveCashback(final List<Result> result);
}
