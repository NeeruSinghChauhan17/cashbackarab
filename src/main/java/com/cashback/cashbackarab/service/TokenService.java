package com.cashback.cashbackarab.service;

public interface TokenService {

	String createUserToken(final Integer userId,final  String appType,final String deviceId);

	boolean verifyUserToken(final Integer userId, final String tokenId,final  String appType, final String deviceId);

	public boolean isUserExistWithApp(final Integer userId,final String appType);

	void deleteAllTokenByUserId(final Integer userId);

	void invalidateToken(final Integer userId, final String appType,final  String deviceId);
}
