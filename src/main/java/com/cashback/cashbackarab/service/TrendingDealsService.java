package com.cashback.cashbackarab.service;

import java.util.List;

import com.cashback.cashbackarab.models.BrandReviews;
import com.cashback.cashbackarab.models.brand.Brand;


public interface TrendingDealsService {
	
	List<Brand> getDealsByCategory(final Integer userId,final Integer categoryId);
	
	List<Brand> getDealsByCategoryAndCountry(final Integer userId,final Integer categoryId,final  Integer countryId);

	
	
}
