package com.cashback.cashbackarab.service;

import java.util.Optional;

import com.cashback.cashbackarab.content.CashbackApp;
import com.cashback.cashbackarab.content.FirebaseCustomToken;

public interface ContentService {

	Boolean initializeFirebaseApp(final CashbackApp app);

	Optional<FirebaseCustomToken> getFirebaseCustomToken(final Integer userId);
}
