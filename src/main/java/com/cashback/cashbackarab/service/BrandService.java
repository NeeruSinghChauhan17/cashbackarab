package com.cashback.cashbackarab.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.cashback.cashbackarab.models.Action;
import com.cashback.cashbackarab.models.Banner;
import com.cashback.cashbackarab.models.BrandReviews;
import com.cashback.cashbackarab.models.Review;
import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.models.brand.Coupon;
import com.cashback.cashbackarab.models.brand.ShoppingTrips;

public interface BrandService {
	List<Brand> getTrendingDetails(final Integer userId, final Integer countryId);

	List<Brand> getAllTrendingDetails();

	List<Brand> getTrendingStores(final Integer userId, final Integer countryId);

	List<Brand> getAllTrendingStores(final Integer userId);

	List<Brand> getSameCategoryBrands(final Integer userId, final Integer brandId);

	List<Coupon> getCouponsByBrandId(final Integer userId, final Integer brandId);

	List<Brand> getCoupons();

	List<Brand> getCouponsByCountry(final Integer countryId);

	List<Brand> getCouponsByCategory(final Integer caregoryId);

	List<Brand> getCouponsByCategoryAndCountry(final Integer caregoryId, final Integer countryId);

	// List<Brand> getTrendingOfferImages(Integer countryId);

	List<Brand> getFeaturedDeals();

	void activateDeal(final Integer userId, final Integer brandId, final Integer couponId);

	Optional<Brand> getBrandDeatils(final Integer userId, final Integer brandId);

	Map<String, Object> getSearchDetails(final Integer userId, final String brandName);

	void addOrRemoveStoreFromFavourite(final Integer brandId, final Integer userId, final Action action);

	List<Brand> getFavouriteBrands(final Integer userId);

	List<ShoppingTrips> getShoppingTrips(final Integer userId);

	List<Banner> getBannerImages();

	void addReview(final Integer userId, final Review review);

	BrandReviews getReviewsByBrandId(final Long brandId);
}
