package com.cashback.cashbackarab.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.cashback.cashbackarab.exception.AccountLockedException;
import com.cashback.cashbackarab.models.CashBack;
import com.cashback.cashbackarab.models.ChangePassword;
import com.cashback.cashbackarab.models.Country;
import com.cashback.cashbackarab.models.InternationalNumber;
import com.cashback.cashbackarab.models.Login;
import com.cashback.cashbackarab.models.Profile;
import com.cashback.cashbackarab.models.UpdatePassword;
import com.cashback.cashbackarab.models.User;

public interface UserServiceV2 {

	User register(final User user, final String userAgent, final String deviceToken, final String deviceType, final Integer refId);

	User login(final Login login, final String userAgent, final String deviceToken, final String deviceType) throws AccountLockedException;

	User socialLogin(final String socialId, final String userAgent, final String deviceToken, final String deviceType);

	void verifyEmail(final Long userId);

	void forgotPassword(final String emailId);

	void resendVerificationMail(final String emailId);

	List<Country> getAllCountries();

	User getEmailBySocialId(final String socialId);

	Profile getProfileById(final Integer userId);

	Profile updateUserProfile(final Integer userId, final Profile userProfile);

	String updatePassword(final Integer userId, final ChangePassword changePassword, final String userAgent,
			final String deviceType);

	void updateNotificatioStatus(final Integer userId, final Integer status);

	void updatePushToken(final Integer userId, final String deviceToken, final String deviceType);

	void logout(final Integer userId, final String userAgent, final String deviceType);

	void updateProfilePic(final Integer userId, final String imageUrl);

	List<CashBack> getCashbackHistory(final Integer userId);

	
	Collection<Map> getSettingValues();

	void verifyPhoneNumber(final InternationalNumber number);

	void forgetUpdatePassword( final UpdatePassword updatePassword);

	boolean isNoExists(final InternationalNumber number);

	void updateNumber(Integer userId,InternationalNumber number);
}
