package com.cashback.cashbackarab.service;

import com.cashback.cashbackarab.models.notification.Push;

public interface NotificationService {
	
	void sendNotification(final Integer userId,final  Push push);          
	void broadCast(final Push push);
}
