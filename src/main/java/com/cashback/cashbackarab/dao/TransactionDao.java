package com.cashback.cashbackarab.dao;

import java.util.List;
import java.util.Optional;

import com.cashback.cashbackarab.models.http.Result;
import com.cashback.cashbackarab.models.transaction.Payment;
import com.cashback.cashbackarab.models.transaction.RedeemCashback;


public interface TransactionDao {

	void giveSignupBonus(final Integer userId);

	Double getUserCashbackBalance(final Integer userId);

	Double getTotalCashbackAmount(final Integer userId);

	Double getTotalWithdrawlAmount(final Integer userId);

	String getSettingValue(final String key);

	List<Payment> getPaymentMethods();

	void redeemCashback(final RedeemCashback redeemCashback,final  Integer userId);

	Optional<String> isRefrenceIdExist(final String refrenceId);
	
	void giveReferFriendBonus(final Integer refId);
	
	Integer countReferFriendBonus(final Integer refId);

	void giveCashback(final List<Result> results);
}
