package com.cashback.cashbackarab.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.cashback.cashbackarab.models.CashBack;
import com.cashback.cashbackarab.models.ChangePassword;
import com.cashback.cashbackarab.models.Country;
import com.cashback.cashbackarab.models.DeviceInfo;
import com.cashback.cashbackarab.models.InternationalNumber;
import com.cashback.cashbackarab.models.Login;
import com.cashback.cashbackarab.models.Profile;
import com.cashback.cashbackarab.models.UpdatePassword;
import com.cashback.cashbackarab.models.User;
import com.google.common.base.Optional;

public interface UserDaoV2 {
	/*Optional<User> isExists(final User usr);*/
	Optional<User> isUserExistsWithEmail(final String email);
	 Optional<User> isUserExistsWithNo(final InternationalNumber no);
	Optional<User> isSocialIdExist(final String socialId);

	User save(final User user, final Integer refId);

	void verifyEmail(final Long userId);

	User updateUser(final Integer userId, final User user, final Boolean isHavePassword);

	Optional<User> getUserDetailsByPhone(final InternationalNumber number);

	Optional<User> getUserDetailsbySocialId(final String socialId);

	/*
	 * Boolean checkAccountStatus(final String emailId, final String password);
	 */

	void updateToken(final String emailId, final String token);

	void updatePassword(final String emailId, final String password);

	List<Country> getAllCountries();

	Optional<Profile> getProfileById(final Integer userId);

	void updateuserProfile(final Integer userId, final Profile userProfile);

	void changePassword(final Integer userId, final ChangePassword changePassword);

	void updateNotificationStatus(final Integer userId, final Integer status);

	void updatePushToken(final Integer userId, final String deviceToken, final String deviceType);

	void updateProfilePic(final Integer userId, final String imageUrl);

	Optional<User> findUserById(final Integer userId);

	Optional<DeviceInfo> findLastActiveDeviceInfoById(final Integer userId);

	List<CashBack> getCashbackHistory(final Integer userId);

	Collection<Map> getSettingValues();

	List<DeviceInfo> getDevicesInfo(final Integer offset,final Integer limit);
	
	boolean phoneNumberVerified(InternationalNumber number);
	
	public Optional<User> getUserDetailsByEmail(final Login login);
	
	boolean isNoExists(InternationalNumber number);
	
	 void forgetUpdatePassword(UpdatePassword password);
	 
	boolean isSocialUserWithUserId(Integer userId);
	
	boolean updateNumber(Integer userId,InternationalNumber number);
	
	void deleteNumberFromOldUser(InternationalNumber number);
	

}
