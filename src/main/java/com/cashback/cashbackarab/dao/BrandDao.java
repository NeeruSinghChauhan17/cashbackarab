package com.cashback.cashbackarab.dao;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.cashback.cashbackarab.models.Action;
import com.cashback.cashbackarab.models.Banner;
import com.cashback.cashbackarab.models.Review;
import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.models.brand.Category;
import com.cashback.cashbackarab.models.brand.Coupon;
import com.cashback.cashbackarab.models.brand.ShoppingTrips;

public interface BrandDao {
	List<Brand> getTrendingDeals(final Integer userId, final Integer countryId);

	List<Brand> getTrendingStores(final Integer userId, final Integer countryId);

	List<Brand> getAllTrendingStores(final Integer userId);

	List<Brand> getSameCategoryBrands(final Integer userId, final Integer brandId);

	List<Brand> getStoresByCategory(final Integer userId, final Integer categoryId);

	List<Coupon> getCouponsByBrandId(final Integer userId, final Integer brandId);

	List<Brand> getCoupons();

	List<Brand> getCouponsByCountry(final Integer countryId);

	List<Brand> getCouponsByCategory(final Integer categoryId);

	List<Brand> getCouponsByCategoryAndCountry(final Integer categoryId, final Integer countryId);

	List<Brand> getTrendingOfferImages(final Integer countryId);

	List<Brand> getFeaturedDeals();

	void activateDeal(final Integer userId, final Integer brandId, final Integer couponId);

	java.util.Optional<Brand> getBrandDetails(final Integer userId, final Integer brandId);

	Map<String, Object> getsearchDetails(final Integer userId, final String brandName);

	void addOrRemoveStoreFromFavourite(final Integer brandId, final Integer userId, final Action action);

	List<Brand> getFavouriteBrands(final Integer userId);

	List<ShoppingTrips> getShoppingTrips(final Integer userId);

	List<Banner> getBannerImages();

	List<Category> getCategoryId(final Integer brandId);

	void saveClickHistory(final Integer userId, final Integer brandId);

	void increaseBrandVisits(final Integer brandId);

	void increaseCouponVisits(final Integer couponId);

	List<Brand> getAllTrendingDeals();

	List<Review> getReviewsByBrandId(final Long brandId);

	void addReview(final Integer userId, final Review review);

	void deleteExistingReviewifExist(final Integer userId, final Review review);

	Optional<Float> getReviewsAverageRating(final Long brandId);

}
