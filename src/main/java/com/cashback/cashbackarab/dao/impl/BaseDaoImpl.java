package com.cashback.cashbackarab.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.cashback.cashbackarab.dao.BaseDaO;
import com.cashback.cashbackarab.models.Review;

public class BaseDaoImpl implements BaseDaO {

	private final NamedParameterJdbcOperations jdbcTemplate;

	private final Map<String, String> queryMap;

	private static final Logger logger = LoggerFactory.getLogger(BaseDaoImpl.class);

	public BaseDaoImpl(final Map<String, String> queryMap, final NamedParameterJdbcOperations jdbcTemplate) {
		this.queryMap = queryMap;
		this.jdbcTemplate = jdbcTemplate;
	}  

	@Override
	public NamedParameterJdbcOperations getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	public String getQueryById(final String id) {
		return queryMap.get(id);
	}

	@Override
	public int insert(final String query,final  Map<String, Object> paramMap) {
		return jdbcTemplate.update(query, paramMap);
	}

	@Override
	public int getRowCount(final String query,final  SqlParameterSource source) {
		try {
			return jdbcTemplate.queryForObject(query, source, Integer.class);
		} catch (EmptyResultDataAccessException e) {
			logger.error("empty result", e);
			return 0;
		}
	}
	


}
