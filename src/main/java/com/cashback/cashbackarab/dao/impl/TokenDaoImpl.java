package com.cashback.cashbackarab.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.sql.Timestamp;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.cashback.cashbackarab.dao.BaseDaO;
import com.cashback.cashbackarab.dao.TokenDao;
import com.cashback.cashbackarab.dao.rowmapper.TokenRowMapper;
import com.cashback.cashbackarab.exception.UnAuthorizedException;
import com.cashback.cashbackarab.models.Token;
import com.google.common.base.Optional;

@Repository
public class TokenDaoImpl implements TokenDao {

	private static final RowMapper<Token> TOKEN_ROW_MAPPER = new TokenRowMapper();
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenDaoImpl.class);
	private final BaseDaO baseDao;

	@Autowired
	public TokenDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("token.xml"), "Base Dao Cannot Be Null");
	}

	@Override
	public void saveUserToken(final Token token, final String appType,final String deviceId) {
		final String query = baseDao.getQueryById("saveUserToken");
		final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("userId", token.getUserId()).addValue("uniqueId", token.getUniqueId())
				.addValue("appType", appType).addValue("deviceId", deviceId)
				.addValue("creationDateTime", new Timestamp(token.getCreationDateTime().getMillis()));
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public void updateUserToken(final Token token,final  String appType, final String deviceId) {
		final String query = baseDao.getQueryById("updateUserToken");
		final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("userId", token.getUserId()).addValue("uniqueId", token.getUniqueId())
				.addValue("appType", appType).addValue("deviceId", deviceId)
				.addValue("creationDateTime", new Timestamp(token.getCreationDateTime().getMillis()));
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public Optional<Token> getUserSession(final Token token,final  String appType,final String deviceId) {
		final String query = baseDao.getQueryById("getTokenInfo");
		final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("userId", token.getUserId()).addValue("uniqueId", token.getUniqueId())
				.addValue("appType", appType).addValue("deviceId", deviceId);
		try {
			Token tokenResp = baseDao.getJdbcTemplate().queryForObject(query, sqlParameterSource, TOKEN_ROW_MAPPER);
			return Optional.fromNullable(tokenResp);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("=======> EmptyResultDataAccessException =========>" + e + "==============>");
			return Optional.<Token>absent();
		} catch (DataAccessException e) {
			LOGGER.info("=======> DataAccessException =========>" + e + "==============>");
			return Optional.<Token>absent();
		}
	}

	@Override
	public Token getUserToken(final Integer userId,final  String appType,final String deviceId) {
		final String query = baseDao.getQueryById("getUserToken");
		final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("appType", appType).addValue("deviceId", deviceId);
		try {
			return baseDao.getJdbcTemplate().queryForObject(query, sqlParameterSource, TOKEN_ROW_MAPPER);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("=======> EmptyResultDataAccessException =========>" + e + "==============>");
			throw new UnAuthorizedException("token not exist");
		} catch (DataAccessException e) {
			throw new UnAuthorizedException("token not exist");
		}

	}

	@Override
	public Collection<Token> isUserTokenExistWithApp(final Integer userId,final String appType) {
		final String query = baseDao.getQueryById("checkUserWithApp");
		final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("appType", appType);
		return baseDao.getJdbcTemplate().query(query, sqlParameterSource, TOKEN_ROW_MAPPER);

	}

	@Override
	public void deleteAllTokenByUserId(final Integer userId) {
		final String query = baseDao.getQueryById("deleteAllTokenByUserId");
		final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("userId", userId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public void invalidateToken(final Integer userId,final  String appType,final String deviceId) {
		final String query = baseDao.getQueryById("invalidateToken");
		final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("appType", appType).addValue("deviceId", deviceId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}
}
