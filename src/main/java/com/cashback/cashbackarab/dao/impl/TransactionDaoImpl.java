package com.cashback.cashbackarab.dao.impl;
import static com.google.common.base.Preconditions.checkNotNull;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.cashback.cashbackarab.dao.BaseDaO;
import com.cashback.cashbackarab.dao.TransactionDao;
import com.cashback.cashbackarab.dao.rowmapper.PaymentRowMapper;
import com.cashback.cashbackarab.models.http.Result;
import com.cashback.cashbackarab.models.notification.Push;
import com.cashback.cashbackarab.models.notification.PushPayload;
import com.cashback.cashbackarab.models.transaction.Payment;
import com.cashback.cashbackarab.models.transaction.RedeemCashback;
import com.cashback.cashbackarab.service.NotificationService;
import com.cashback.cashbackarab.utils.GETCASHBACK;
import com.cashback.cashbackarab.utils.RandomString;


@Repository
public class TransactionDaoImpl implements TransactionDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionDaoImpl.class);
	private static final PaymentRowMapper PAYMENT_ROW_MAPPER = new PaymentRowMapper();
	private final BaseDaO baseDao;
	private  static final String STATUS_CONFIRMED ="confirmed";
	private static final String STATUS_APPROVED ="approved";
	
	@Autowired
	private NotificationService notificationService;

	@Autowired
	public TransactionDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("cashback-transaction.xml"),
				"Base Dao Cannot Be Null");
	}

	@Override
	public String getSettingValue(final String key) {
		final String getSettingValue = baseDao.getQueryById("getSettingValue");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("settingKey", key);
		String SettingValue = baseDao.getJdbcTemplate().queryForObject(getSettingValue, source, String.class);
		return SettingValue;
	}

	@Override
	public void giveSignupBonus(final Integer userId) {
		final String saveUserQuery = baseDao.getQueryById("giveUserSignupBonus");
		String signupValue = getSettingValue("signup_credit");
		Double signupBonus = 0.0000;
		if (signupValue != null) {
			signupBonus = Double.valueOf(signupValue);
		}
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("referenceId", generateReferenceId()).addValue("paymentType", "Sign Up Bonus")
				.addValue("paymentDetails", "").addValue("amount", signupBonus).addValue("status", "confirmed")
				.addValue("dateTime", new Timestamp(DateTime.now().getMillis())).addValue("reason", "");

		baseDao.getJdbcTemplate().update(saveUserQuery, source);
	}

	@Override
	public void giveReferFriendBonus(final Integer refId) {
		Integer refBonusCount = countReferFriendBonus(refId);
		if (refBonusCount < 3) {
			final String giveReferBonusQuery = baseDao.getQueryById("giveReferFriendBonus");
			String referCreditValue = getSettingValue("refer_credit");
			Double referBonus = 0.0000;
			if (referCreditValue != null) {
				referBonus = Double.valueOf(referCreditValue);
			}
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", refId)
					.addValue("referenceId", generateReferenceId()).addValue("paymentType", "Refer a Friend Bonus")
					.addValue("paymentDetails", "").addValue("amount", referBonus).addValue("status", "pending")
					.addValue("dateTime", new Timestamp(DateTime.now().getMillis())).addValue("reason", "");
			baseDao.getJdbcTemplate().update(giveReferBonusQuery, source);
		}
	}

	@Override
	public Integer countReferFriendBonus(final Integer refId) {
		final String countReferBonusQuery = baseDao.getQueryById("countReferFriendBonus");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("refId", refId).addValue("paymentType",
				"Refer a Friend Bonus");
		return baseDao.getJdbcTemplate().queryForObject(countReferBonusQuery, source, Integer.class);

	}

	@Override
	public Double getUserCashbackBalance(final Integer userId) {
		Double totalCashbackAmount = getTotalCashbackAmount(userId);
		Double totalWithdrawlAmount = getTotalWithdrawlAmount(userId);
		Double availableCashback = totalCashbackAmount - totalWithdrawlAmount;
		return availableCashback;
	}

	@Override
	public Double getTotalCashbackAmount(final Integer userId) {
		final String getUserQuery = baseDao.getQueryById("getUserConfirmedCashback");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		Double amount = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, Double.class);
		if (amount != null) {
			return amount;
		}
		return 0.0000;
	}

	@Override
	public Double getTotalWithdrawlAmount(final Integer userId) {
		final String getUserQuery = baseDao.getQueryById("getUserWithdrawalAndPaidCashback");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		Double amount = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, Double.class);
		if (amount != null) {
			return amount;
		}
		return 0.0000;
	}

	@Override
	public List<Payment> getPaymentMethods() {
		final String paymentMethods = baseDao.getQueryById("getpaymentMethods");
		final SqlParameterSource source = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().query(paymentMethods, source, PAYMENT_ROW_MAPPER);

	}

	@Override
	public void redeemCashback(final RedeemCashback redeemCashback,final  Integer userId) {
		final String saveUserQuery = baseDao.getQueryById("InsertTransactionDetails");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("referenceId", generateReferenceId()).addValue("paymentType", "Withdrawal")
				.addValue("paymentDetails", redeemCashback.getPaymentDetails())
				.addValue("paymentMethod", redeemCashback.getPaymentMethod())
				.addValue("amount", redeemCashback.getAmount()).addValue("status", "request")
				.addValue("dateTime", new Timestamp(DateTime.now().getMillis())).addValue("reason", "");
		baseDao.getJdbcTemplate().update(saveUserQuery, source);

	}

	@Override
	public Optional<String> isRefrenceIdExist(final String referenceId) {
		try {
			final String getUserQuery = baseDao.getQueryById("checkReferenceIdExistance");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("referenceId", referenceId);
			String reference = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, String.class);
			return Optional.ofNullable(reference);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public String generateReferenceId() {
		String referenceId = RandomString.generateRefrenceId();
		if (isRefrenceIdExist(referenceId).isPresent()) {
			generateReferenceId();
		}
		return referenceId;
	}

	
	/*
	 * update database in cases:
	 * 1.) old status!=confirmed/approved and new status=confirmed/approved
	 * 2.) old=no data and new status added
	*/
	@Override
	public void giveCashback(final List<Result> rs) {
         final String giveCashbackQuery = baseDao.getQueryById("give.cashback");
		 final SqlParameterSource[] batch = new MapSqlParameterSource[rs.size()];
		 try{
       for (int index = 0; index < rs.size(); index++) {
    	
			final Result res = rs.get(index);
			 Float cashback=res.getPayment();
			 
			cashback= getCashbackInSr(res.getCart(),cashback,res.getCurrency(),res.getSubId3());
			if(ifConfirmedSendNotification( res.getSubId4(),res.getActionId(),res.getAdvcampaignName(),res.getPayment(),res.getStatus())){
				  
				batch[index] = new MapSqlParameterSource()
					      .addValue("referenceId", res.getActionId())
					      .addValue("userId",res.getSubId4())                        
					      .addValue("paymentType", res.getAdvcampaignName())
				          .addValue("paymentMethod", 0)
					      .addValue("paymentDetails", res.getAction())
					      .addValue("amount", cashback)
					       .addValue("status", res.getStatus())
					       .addValue("created", res.getClickDate())
					       .addValue("reason", "")
					       .addValue("processDate", res.getActionDate());
			       
			   }
			 } 
         
        baseDao.getJdbcTemplate().batchUpdate(giveCashbackQuery, batch);
        LOGGER.info("Give cashback-Tracking report saved=============");
		 }
       catch(Exception e){
    	 LOGGER.error("===============Error while Give cashback-batch update=============",e);   
       
	}
          
	};
	
	 /**
	   * if old status is not equal to confirmed/approved and new status is confirmed/approved 
	   * update database and  call notificationSender 
	   */
	

	private Boolean ifConfirmedSendNotification(final String userId,final String actionId,final String advcampaignName,final Float payment,final String status){
	Optional<String> oldstatus=findCashBackStatus( actionId);
	
	      if(oldstatus.isPresent()){    
	    	  if((!(oldstatus.get().equalsIgnoreCase(STATUS_CONFIRMED)) && (status.equalsIgnoreCase(STATUS_CONFIRMED))) ||
	    			 (!(oldstatus.get().equalsIgnoreCase(STATUS_APPROVED)) && (status.equalsIgnoreCase(STATUS_APPROVED)))){
	    		notificationSender(userId,advcampaignName,payment);
	    		return true;
	         	}
	     }
	    
	   else if((status.equalsIgnoreCase(STATUS_CONFIRMED)) || (status.equalsIgnoreCase(STATUS_APPROVED))){
    		 notificationSender(userId,advcampaignName,payment);
    		 return true;
    	}
		return true;
	} 
	
	
	
	private Float getCashbackInSr(Float cart,Float cashback,String currency,String subid3){
		try{
			cashback=GETCASHBACK.getTotalCashback(cart,currency,subid3);          // subId3 can be null before 10jan,2018
			}catch(Exception e){
			LOGGER.error("ERROR(SUB-ID3 IS NOT FOUND) WHILE GET CASHBACK IN SR ACCORDING TO % CASHBACKARAB.COM=============" +e);	
			} 	
		return cashback;
	}

	private void notificationSender(final String userId,final String advcampaignName,final Float payment ){
		try{
			Map<String, String>  customData = new HashMap<String, String>();
			   customData.put("type","5");
			   customData.put("message","كاشباك" + payment  + "فروم" +advcampaignName  + "تمت إضافته إلى حسابك");
			notificationService.sendNotification(Integer.valueOf(userId), Push.withPayload(                    
					            PushPayload.withUserCashbackPayload(customData)));
			LOGGER.info(" Give cashback-notifications sent to status confirmed/approved user of userId ===== "+userId);
	     	}catch(Exception e){
			LOGGER.error("=============Error while  broadcasting of notifications=============",e);	
		}
	}
	
	
	private Optional<String> findCashBackStatus(final String actionId) {
		try{
		final String query = baseDao.getQueryById("find.cashback.status");
		MapSqlParameterSource source = new MapSqlParameterSource()
				.addValue("actionId",actionId);
	 return  Optional.ofNullable(baseDao.getJdbcTemplate().queryForObject(query, source, String.class));
		} catch (EmptyResultDataAccessException e) {
	  LOGGER.error("=============findCashbackStatus-User not exists-For New user for  actionId= "+actionId+"=============  ");	
		return Optional.empty();
	}
  }

}
