package com.cashback.cashbackarab.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.cashback.cashbackarab.annotation.Secured;
import com.cashback.cashbackarab.dao.BaseDaO;
import com.cashback.cashbackarab.dao.BrandDao;
import com.cashback.cashbackarab.dao.rowmapper.BannerRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.BrandCouponsRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.BrandDetailsRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.BrandReviewRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.BrandTrendingDealsRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.BrandTrendingOfferRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.BrandTrendingStoresRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.CategoryRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.CouponRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.ShoppingTripsRowMapper;
import com.cashback.cashbackarab.models.Action;
import com.cashback.cashbackarab.models.Banner;
import com.cashback.cashbackarab.models.Review;
import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.models.brand.Category;
import com.cashback.cashbackarab.models.brand.Coupon;
import com.cashback.cashbackarab.models.brand.ShoppingTrips;

import com.cashback.cashbackarab.utils.DateUtil;


@Repository
public class BrandDaoImpl implements BrandDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(BrandDaoImpl.class);

	private final BaseDaO baseDao;
	private final BrandCouponsRowMapper BRAND_COUPON_ROW_MAPPER = new BrandCouponsRowMapper();
	private final BrandTrendingDealsRowMapper BRAND_TRENDING_DEALS_ROW_MAPPER = new BrandTrendingDealsRowMapper();
	private final BrandTrendingStoresRowMapper BRAND_TRENDING_STORES_ROW_MAPPER = new BrandTrendingStoresRowMapper();
	private final BrandTrendingOfferRowMapper BRAND_TRENDING_OFFER_ROW_MAPPER = new BrandTrendingOfferRowMapper();
	// private final UserDealsActivationRowMapper
	// USER_DEALS_ACTIVATION_ROW_MAPPER = new UserDealsActivationRowMapper();
	private final BrandDetailsRowMapper BRAND_DETAILS_ROW_MAPPER = new BrandDetailsRowMapper();
	private final CategoryRowMapper CATEGORY_ROW_MAPPER = new CategoryRowMapper();
	private static final RowMapper<Coupon> COUPON_ROW_MAPPER = new CouponRowMapper();
	private static final RowMapper<ShoppingTrips> SHOPPING_TRIPS_ROWMAPPER = new ShoppingTripsRowMapper();
	private static final RowMapper<Banner> BANNER_ROW_MAPPER = new BannerRowMapper();
	private final RowMapper<Review> BRAND_REVIEW_ROW_MAPPER = new BrandReviewRowMapper();
	@Autowired
	public BrandDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("cashback-store.xml"), "Base Dao Cannot Be Null");

	}
    
	public List<Brand> getTrendingDeals(final Integer userId, final Integer countryId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.trending.deals.by.country.id");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("countryId", countryId).addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		LOGGER.info(DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_DEALS_ROW_MAPPER);
	}

	/*@Override
	public List<Brand> getAllTrendingDeals(Integer userId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.all.trending.deals.by.user.id");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("timeStamp",
				DateUtil.getCurrentTimeStamp());
		LOGGER.info(DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_DEALS_ROW_MAPPER);
	}*/
	

	@Override
	public List<Brand> getAllTrendingDeals() {
		final String trendingDealsQuery = baseDao.getQueryById("find.all.trending.deals");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("timeStamp",
				DateUtil.getCurrentTimeStamp());
		LOGGER.info(DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_DEALS_ROW_MAPPER);
	}

	public List<Brand> getTrendingStores(final Integer userId, final Integer countryId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.trending.stores.by.country.id");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("countryId", countryId)
				.addValue("timeStamp", DateUtil.convertToDateString(Instant.now().toEpochMilli()));
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_STORES_ROW_MAPPER);
	}

	@Override
	public List<Brand> getAllTrendingStores(final Integer userId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.all.trending.stores.by.user.id");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("timeStamp",
				DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_STORES_ROW_MAPPER);
	}
	
	

	@Override
	public List<Brand> getSameCategoryBrands(final Integer userId,final  Integer brandId) {
		List<Category> categories = getCategoryId(brandId);
		List<Brand> brandList = new ArrayList<Brand>();
		Map<Integer, Brand> brandMap = new HashMap<Integer, Brand>();
		for (Category category : categories) {
			brandList.addAll(getStoresByCategory(userId, category.getCategory_id()));
		}
		for (Brand brand : brandList) {
			brandMap.put(brand.getBranId(), brand);
		}
		return new ArrayList<>(brandMap.values());
	}

	@Override
	public List<Brand> getStoresByCategory(final Integer userId,final  Integer categoryId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.stores.by.category");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("categoryId", categoryId)
				.addValue("userId", userId).addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_STORES_ROW_MAPPER);
	}

	@Override
	public List<Category> getCategoryId(final Integer brandId) {
		final String getBrandCategory = baseDao.getQueryById("getBrandCategories");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("brandId", brandId);
		return baseDao.getJdbcTemplate().query(getBrandCategory, source, CATEGORY_ROW_MAPPER);
	}

	@Override
	public List<Coupon> getCouponsByBrandId(final Integer userId,final Integer brandId) {
		final String couponCodeQuery = baseDao.getQueryById("find.coupons.by.brand.id");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("timeStamp", DateUtil.getCurrentTimeStamp())
				.addValue("brandId",
				brandId);
		return baseDao.getJdbcTemplate().query(couponCodeQuery, source, COUPON_ROW_MAPPER);
	}

	@Override
	public List<Brand> getCoupons() {
		final String couponCodeQuery = baseDao.getQueryById("find.all.coupons");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("timeStamp",
				DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(couponCodeQuery, source, BRAND_COUPON_ROW_MAPPER);
	}

	@Override
	public List<Brand> getCouponsByCountry(final Integer countryId) {
		final String couponCodeQuery = baseDao.getQueryById("find.all.coupons.by.country");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("countryId", countryId)
				.addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(couponCodeQuery, source, BRAND_COUPON_ROW_MAPPER);
	}

	@Override
	public List<Brand> getCouponsByCategory(final Integer categoryId) {
		final String couponCodeQuery = baseDao.getQueryById("find.all.coupons.by.category");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("categoryId", categoryId)
				.addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		LOGGER.info("date  : " + DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(couponCodeQuery, source, BRAND_COUPON_ROW_MAPPER);
	}

	@Override
	public List<Brand> getCouponsByCategoryAndCountry(final Integer categoryId,final  Integer countryId) {
		final String couponCodeQuery = baseDao.getQueryById("find.all.coupons.by.category.and.country");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("categoryId", categoryId)
				.addValue("countryId", countryId).addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		LOGGER.info("date  : " + DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(couponCodeQuery, source, BRAND_COUPON_ROW_MAPPER);
	}

	public List<Brand> getTrendingOfferImages(final Integer countryId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.trending.offers.images.by.country.id");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("countryId", countryId)
				.addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_OFFER_ROW_MAPPER);
	}

	@Override
	public List<Brand> getFeaturedDeals() {
		final String trendingDealsQuery = baseDao.getQueryById("find.featured.deals");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_DEALS_ROW_MAPPER);
	}

	@Secured
	@Override
	public void activateDeal(final Integer userId, final Integer brandId, final Integer couponId) {
		if (brandId != null) {
			saveClickHistory(userId, brandId);
			increaseBrandVisits(brandId);
		}
		if (couponId != null) {
			increaseCouponVisits(couponId);
		}
	}

	@Override
	public void saveClickHistory(final Integer userId, final Integer brandId) {
		final String saveClickHistory = baseDao.getQueryById("save.user.click.history");
        final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("brandId", brandId).addValue("now", new Timestamp(DateTime.now().getMillis()));
		baseDao.getJdbcTemplate().update(saveClickHistory, source);
	}

	@Override
	public void increaseBrandVisits(final Integer brandId) {
		final String saveClickHistory = baseDao.getQueryById("increase.brand.visits");
        final SqlParameterSource source = new MapSqlParameterSource().addValue("brandId", brandId);
		baseDao.getJdbcTemplate().update(saveClickHistory, source);
	}

	@Override
	public void increaseCouponVisits(final Integer couponId) {
		final String saveClickHistory = baseDao.getQueryById("increase.coupon.visits");
       final SqlParameterSource source = new MapSqlParameterSource().addValue("couponId", couponId);
		baseDao.getJdbcTemplate().update(saveClickHistory, source);
	}

	@Override
	public Optional<Brand> getBrandDetails(final Integer userId, final Integer brandId) {
		try {
			final String findBrandDetails = baseDao.getQueryById("find.brand.details.by.brandId.userId");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("brandId", brandId)
					.addValue("userId", userId);
			final Brand brand = baseDao.getJdbcTemplate().queryForObject(findBrandDetails, source,
					BRAND_DETAILS_ROW_MAPPER);
			return Optional.ofNullable(brand);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
			}
	}
	


	@Override
	public Map<String, Object> getsearchDetails(final Integer userId, final String brandName) {
		Map<String, Object> searchDetails = new HashMap<String, Object>();
		List<Brand> brandList = searchStoresByName(userId, brandName);
		List<Brand> brandListByCategory = searchStoreByCategory(userId, brandName);
		brandList.addAll(brandListByCategory);
		Map<Integer, Brand> searchList = new HashMap<Integer, Brand>();
		for (Brand brand : brandList) {
			searchList.put(brand.getBranId(), brand);
		}
		searchDetails.put("stores", searchList.values());
		return searchDetails;
	}

	public List<Brand> searchStoresByName(final Integer userId,final String brandName) {
		
		final String trendingDealsQuery = baseDao.getQueryById("Search.trending.stores.by.string");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("brandName", brandName + "%").addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_STORES_ROW_MAPPER);
	}

	public List<Brand> searchStoreByCategory(final Integer userId,final  String CategoryName) {
		
		final String trendingDealsQuery = baseDao.getQueryById("Search.store.by.category.name");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("CategoryName", CategoryName + "%").addValue("timeStamp", DateUtil.getCurrentTimeStamp());
		LOGGER.info(DateUtil.getCurrentTimeStamp());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_STORES_ROW_MAPPER);
	}

	/*
	 * public List<Coupon> searchCouponsByBrandName(Integer userId, String
	 * brandName) { final String couponCodeQuery = baseDao
	 * .getQueryById("Search.coupons.by.storename"); final SqlParameterSource
	 * source = new MapSqlParameterSource().addValue("userId", userId).addValue(
	 * "brandName", "%"+brandName+"%"); return
	 * baseDao.getJdbcTemplate().query(couponCodeQuery, source,
	 * COUPON_ROW_MAPPER); }
	 */

	@Override
	public void addOrRemoveStoreFromFavourite(final Integer brandId, final Integer userId,final Action action) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("brandId", brandId).addValue("addedTime", DateUtil.getCurrentTimeStamp());
		if (Action.getIntValue(action) == 1) {
			baseDao.getJdbcTemplate().update(baseDao.getQueryById("removeBrandFromFavourite"), source);
			baseDao.getJdbcTemplate().update(baseDao.getQueryById("addBrandToFavourite"), source);
         } else {
			baseDao.getJdbcTemplate().update(baseDao.getQueryById("removeBrandFromFavourite"), source);
		}
	}

	@Override
	public List<Brand> getFavouriteBrands(final Integer userId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.user.favourite.brand");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("timeStamp",
				DateUtil.convertToDateString(Instant.now().toEpochMilli()));
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_STORES_ROW_MAPPER);
	}

	@Override
	public List<ShoppingTrips> getShoppingTrips(final Integer userId) {
        final String shoppingTrips = baseDao.getQueryById("find.user.shopping.trips");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		return baseDao.getJdbcTemplate().query(shoppingTrips, source, SHOPPING_TRIPS_ROWMAPPER);

	}

	@Override
	public List<Banner> getBannerImages() {
		final String shoppingTrips = baseDao.getQueryById("get.banner.images");
		final SqlParameterSource source = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().query(shoppingTrips, source, BANNER_ROW_MAPPER);
	}

	
	@Override
	public List<Review> getReviewsByBrandId(final Long brandId) {
		final String getReviewsQuery = baseDao.getQueryById("get.reviews.by.brand.id");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("brandId", brandId);
         LOGGER.info("current date " + DateTime.now());
		return baseDao.getJdbcTemplate().query(getReviewsQuery, source, BRAND_REVIEW_ROW_MAPPER);
	}

	@Override
	public Optional<Float> getReviewsAverageRating(final Long brandId) {
		final String getReviewsRatingQuery = baseDao.getQueryById("get.reviews.rating.by.brand.id");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("brandId", brandId);
          LOGGER.info("current date " + DateTime.now());
		Float avgRating= baseDao.getJdbcTemplate().queryForObject(getReviewsRatingQuery, source,Float.class);
		return Optional.ofNullable((avgRating==null)?0:avgRating);
		
	}
	
	@Override
	public void addReview(final Integer userId, final Review review) {
		final String saveUserQuery = baseDao.getQueryById("add.review");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("brandId", review.getBrandId()).addValue("title", review.getTitle())
				.addValue("text", (review.getText()==null)?"":review.getText())
				.addValue("rating", review.getRating())
				.addValue("added", new Timestamp(DateTime.now().getMillis()))
				.addValue("updated", new Timestamp(DateTime.now().getMillis()));
		baseDao.getJdbcTemplate().update(saveUserQuery, source);
	}
	
	@Override
	public void deleteExistingReviewifExist(final Integer userId, final Review review) {
		final String deleteReviewQuery = baseDao.getQueryById("delete.review");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("brandId", review.getBrandId())
				.addValue("userId", userId);
      baseDao.getJdbcTemplate().update(deleteReviewQuery, source);
		
	}
}
