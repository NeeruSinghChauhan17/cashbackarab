package com.cashback.cashbackarab.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.cashback.cashbackarab.dao.BaseDaO;
import com.cashback.cashbackarab.dao.MailDao;
import com.cashback.cashbackarab.dao.rowmapper.EmailRowMapper;
import com.cashback.cashbackarab.models.Email;

@Repository
public class EmailDaoImpl implements MailDao{
 
	private static final EmailRowMapper EMAIL_ROW_MAPPER = new EmailRowMapper();
	private final BaseDaO baseDao;

	@Autowired
	public EmailDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("email.xml"),
				"Base Dao Cannot Be Null");
	}
	@Override
	public Email getEmailContent(final String emailName) {
		final String getSettingValue = baseDao.getQueryById("getEmailContent");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("emailName", emailName);
		return baseDao.getJdbcTemplate().queryForObject(getSettingValue, source, EMAIL_ROW_MAPPER);
	}
}
