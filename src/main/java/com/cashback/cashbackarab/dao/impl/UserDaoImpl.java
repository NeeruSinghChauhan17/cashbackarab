package com.cashback.cashbackarab.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.cashback.cashbackarab.dao.BaseDaO;
import com.cashback.cashbackarab.dao.UserDao;
import com.cashback.cashbackarab.dao.rowmapper.CashBackRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.CountryRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.DeviceInfoRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.SettingRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.UserExistanceRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.UserProfileRowMapper;
import com.cashback.cashbackarab.dao.rowmapper.UserRowMapper;
import com.cashback.cashbackarab.exception.ConflictException;
import com.cashback.cashbackarab.exception.NotFoundException;
import com.cashback.cashbackarab.models.CashBack;
import com.cashback.cashbackarab.models.ChangePassword;
import com.cashback.cashbackarab.models.Country;
import com.cashback.cashbackarab.models.DeviceInfo;
import com.cashback.cashbackarab.models.Login;
import com.cashback.cashbackarab.models.Profile;
import com.cashback.cashbackarab.models.User;
import com.cashback.cashbackarab.utils.RandomString;
import com.google.common.base.Optional;

@Repository
public class UserDaoImpl implements UserDao {

	private final BaseDaO baseDao;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);
	private final CountryRowMapper COUNTRY_ROW_MAPPER = new CountryRowMapper();
	private static final String[] USER_ID_COLUMN_NAME = { "user_id" };
	private final UserExistanceRowMapper USER_EXISTANCE_ROW_MAPPER = new UserExistanceRowMapper();
	private final UserRowMapper USER_ROW_MAPPER = new UserRowMapper();
	private final UserProfileRowMapper USER_PROFILE_ROW_MAPPER = new UserProfileRowMapper();
	private final DeviceInfoRowMapper DEVICE_INFO_ROW_MAPPER = new DeviceInfoRowMapper();
	private final CashBackRowMapper CASHBACK_ROW_MAPPER = new CashBackRowMapper();
	private final SettingRowMapper SETTING_ROW_MAPPER = new SettingRowMapper();

	@Autowired
	public UserDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("user.xml"), "Base Dao Cannot Be Null");

	}

	@Override
	public Optional<User> isExists(final String emailId) {
		try {
			final String getUserQuery = baseDao.getQueryById("checkUserExistance");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("emailId", emailId);
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_EXISTANCE_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User>absent();
		}
	}

	@Override
	public Optional<Profile> getProfileById(final Integer userId) {
		try {
			final String getUserQuery = baseDao.getQueryById("getUserProfile");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
			final Profile userProfile = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source,
					USER_PROFILE_ROW_MAPPER);
			return Optional.fromNullable(userProfile);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<Profile>absent();
		}
	}

	@Override
	public void updateuserProfile(final Integer userId,final  Profile userProfile) {
		final SqlParameterSource source = new MapSqlParameterSource()
				 .addValue("userId", userId)
				.addValue("fname", userProfile.getFname()).addValue("lname", userProfile.getLname())
				.addValue("email", userProfile.getEmail()).addValue("phone", userProfile.getPhone())
				.addValue("address", userProfile.getAddress()).addValue("state", userProfile.getState())
				.addValue("city", userProfile.getCity()).addValue("country", userProfile.getCountry())
				.addValue("zip", userProfile.getZip());
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("updateUserProfile"), source);
		if (updated == 0) {
			throw new NotFoundException("user not exists");
		}
	}

	@Override
	public Optional<User> isSocialIdExist(final String socialId) {
		try {
			final String getUserQuery = baseDao.getQueryById("checkSocialIdExistance");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("socialId", socialId);
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_EXISTANCE_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User>absent();
		}
	}

	@Override
	public User save(final User user, final Integer refId) {
		Integer referenceId = 0;
		if (refId != null) {
			referenceId = refId;
		}
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		final String saveUserQuery = baseDao.getQueryById("saveUserAccountInfo");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("fname", user.getFname())
				.addValue("lname", user.getLname()).addValue("email", user.getEmailId())
				.addValue("social_id", user.getSocialId())
				.addValue("password", RandomString.encryptPassword(user.getPassword()))
				.addValue("country", (user.getCountry() == null ? 0 : user.getCountry()))
				.addValue("block_reason", "")
				.addValue("now", new Timestamp(DateTime.now().getMillis())).addValue("refId", referenceId)
				.addValue("status", "inactive");
		try {
			baseDao.getJdbcTemplate().update(saveUserQuery, source, keyHolder, USER_ID_COLUMN_NAME);
		} catch (DataIntegrityViolationException e) {
			LOGGER.error("already exist ", e);
			throw new ConflictException("User already exist. Please login.");
		}
		final Integer userId = keyHolder.getKey().intValue();
		return User.userWithId(user, userId);
	}

	@Override
	public User updateUser(final Integer userId,final  User user,final Boolean isHavePassword) {
		final String saveUserQuery = baseDao.getQueryById("updateUserAccountInfo");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("fname", user.getFname())
				.addValue("lname", user.getLname()).addValue("email", user.getEmailId())
				.addValue("social_id", user.getSocialId())
				.addValue("password", RandomString.encryptPassword(user.getPassword()))
				.addValue("country", user.getCountry()).addValue("block_reason", "").addValue("user_id", userId);
		baseDao.getJdbcTemplate().update(saveUserQuery, source);
		return User.userWithId(user, userId);
	}

	@Override
	public void verifyEmail(final Long userId) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("verifyEmailByUserId"), source);
		if (updated == 0) {
			throw new NotFoundException("user not exists");
		}
	}

	@Override
	public Optional<User> getUserDetails(final Login login) {
	try {
			final String getUserQuery = baseDao.getQueryById("getUserByEmail");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("emailId", login.getEmailId());
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User>absent();
		}
	}

	@Override
	public Optional<User> getUserDetailsbySocialId(final String socialId) {
		// TODO Auto-generated method stub
		try {
			final String getUserQuery = baseDao.getQueryById("getUserBySocialId");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("socialId", socialId);
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User>absent();
		}
	}

	/*
	 * @Override public Boolean checkAccountStatus(String emailId, String
	 * password) { // TODO Auto-generated method stub try { final String query =
	 * baseDao.getQueryById("checkAccountStatus"); final SqlParameterSource
	 * source = new MapSqlParameterSource().addValue("emailId", emailId)
	 * .addValue("password", password);
	 * 
	 * return baseDao.getJdbcTemplate().queryForObject(query, source,
	 * Boolean.class); } catch (EmptyResultDataAccessException e) { return
	 * false; } }
	 */

	@Override
	public void updateToken(final String emailId, final String token) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("emailId", emailId).addValue("token",
				token);
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("updateToken"), source);
		if (updated == 0) {
			throw new NotFoundException("user not exists");
		}
	}

	@Override
	public void updatePassword(final String emailId,final  String password) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("emailId", emailId).addValue("password",
				password);
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("updatePassword"), source);
		if (updated == 0) {
			throw new NotFoundException("Email does not exists");
		}
	}

	@Override
	public List<Country> getAllCountries() {
		final String getAllCountries = baseDao.getQueryById("getAllCountries");
		final SqlParameterSource source = new MapSqlParameterSource();
		return baseDao.getJdbcTemplate().query(getAllCountries, source, COUNTRY_ROW_MAPPER);
	}

	@Override
	public void changePassword(final Integer userId, final ChangePassword changePassword) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("newPassword",
				RandomString.encryptPassword(changePassword.getNewPassword()));
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("changePassword"), source);
		if (updated == 0) {
			throw new NotFoundException("Email does not exists");
		}
	}

	@Override
	public void updateNotificationStatus(final Integer userId,final  Integer status) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("status",
				status);
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("updateNotificationStatus"), source);
		if (updated == 0) {
			throw new NotFoundException("user not exists");
		}
	}

	@Override
	public void updatePushToken(final Integer userId, final String deviceToken,final String deviceType) {
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("userId", userId)
				.addValue("deviceToken", deviceToken)
				.addValue("appType", deviceType)
				.addValue("now", new Timestamp(DateTime.now().getMillis()));
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("updatePushToken"), source);
		if (updated == 0) {
			throw new NotFoundException("user not exists");
		}
	}

	@Override
	public void updateProfilePic(final Integer userId, final String imageUrl) {
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId).addValue("profilePic",
				imageUrl);
		final int updated = baseDao.getJdbcTemplate().update(baseDao.getQueryById("updateProfilePic"), source);
		if (updated == 0) {
			throw new NotFoundException("user not exists");
		}
	}

	@Override
	public Optional<User> findUserById(final Integer userId) {
		try {
			final String getUserQuery = baseDao.getQueryById("findUserByID");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
			final User user = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source, USER_ROW_MAPPER);
			return Optional.fromNullable(user);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<User>absent();
		}
	}

	@Override
	public Optional<DeviceInfo> findLastActiveDeviceInfoById(final Integer userId) {
		try {
			final String getUserQuery = baseDao.getQueryById("getLastActivatedDeviceInfo");
			final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
			final DeviceInfo deviceInfo = baseDao.getJdbcTemplate().queryForObject(getUserQuery, source,
					DEVICE_INFO_ROW_MAPPER);
			return Optional.fromNullable(deviceInfo);
		} catch (EmptyResultDataAccessException e) {
			return Optional.<DeviceInfo>absent();
		}
	}

	@Override
	public List<CashBack> getCashbackHistory(final Integer userId) {
		final String getCashbackHistory = baseDao.getQueryById("getCahbackHistory");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("userId", userId);
		return baseDao.getJdbcTemplate().query(getCashbackHistory, source, CASHBACK_ROW_MAPPER);
	}



	@Override
	public Collection<Map> getSettingValues() {
		final String getSettingQuery = baseDao.getQueryById("get.setting.values");
		return baseDao.getJdbcTemplate().query(getSettingQuery, SETTING_ROW_MAPPER);
	}

	

	@Override
	public List<DeviceInfo> getDevicesInfo(final Integer offset,final Integer limit ) {
		final String getDevicesInfoQuery = baseDao.getQueryById("get.devices.info");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("limit",limit)
				.addValue("offset",offset);
		return  baseDao.getJdbcTemplate().query(getDevicesInfoQuery , source,DEVICE_INFO_ROW_MAPPER);
	}

}
