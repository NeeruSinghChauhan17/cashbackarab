package com.cashback.cashbackarab.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.cashback.cashbackarab.dao.BaseDaO;
import com.cashback.cashbackarab.dao.TrendingDealsDao;
import com.cashback.cashbackarab.dao.rowmapper.BrandTrendingDealsRowMapper;
import com.cashback.cashbackarab.models.brand.Brand;



@Repository
public class TrendingDealsDaoImpl implements TrendingDealsDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrendingDealsDaoImpl.class);
	private final BaseDaO baseDao;
	private final RowMapper<Brand> BRAND_TRENDING_DEALS_ROW_MAPPER = new BrandTrendingDealsRowMapper();
	

	@Autowired
	public TrendingDealsDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("trending-deals.xml"), "Base Dao Cannot Be Null");

	}

	@Override
	public List<Brand> getDealsByCategory(final Integer userId, final Integer categoryId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.trending.deals.by.category.id");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("categoryId", categoryId)
				.addValue("userId", userId).addValue("timeStamp", DateTime.now());
		LOGGER.info("current date " + DateTime.now());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_DEALS_ROW_MAPPER);
	}

	@Override
	public List<Brand> getDealsByCategoryAndCountry(final Integer userId,final Integer categoryId,final Integer countryId) {
		final String trendingDealsQuery = baseDao.getQueryById("find.trending.deals.by.category.id.and.country");
		final SqlParameterSource source = new MapSqlParameterSource().addValue("categoryId", categoryId)
				.addValue("userId", userId).addValue("countryId", countryId).addValue("timeStamp", DateTime.now());
		LOGGER.info("current date " + DateTime.now());
		return baseDao.getJdbcTemplate().query(trendingDealsQuery, source, BRAND_TRENDING_DEALS_ROW_MAPPER);
	}

	

}
