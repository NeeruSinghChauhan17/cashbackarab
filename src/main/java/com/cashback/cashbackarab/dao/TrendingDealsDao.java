package com.cashback.cashbackarab.dao;

import java.util.List;
import java.util.Optional;

import com.cashback.cashbackarab.models.Review;
import com.cashback.cashbackarab.models.brand.Brand;

public interface TrendingDealsDao {

	List<Brand> getDealsByCategory(final Integer userId,final  Integer categoryId);

	List<Brand> getDealsByCategoryAndCountry(final Integer userId,final  Integer categoryId,final  Integer countryId);

	

	
}
