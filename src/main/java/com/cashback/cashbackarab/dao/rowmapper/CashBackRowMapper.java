package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.CashBack;

public class CashBackRowMapper implements RowMapper<CashBack> {

	@Override
	public CashBack mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new CashBack(rs.getDouble("amount"), rs.getString("payment_type"), rs.getString("status"),
				new DateTime(rs.getTimestamp("created")));
	}

}
