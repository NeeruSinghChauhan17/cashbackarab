package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.Country;

public class CountryRowMapper implements RowMapper<Country> {

	@Override
	public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Country(rs.getInt("country_id"), rs.getString("code"), rs.getString("name"));
	}

}
