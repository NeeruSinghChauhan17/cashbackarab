package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.brand.ShoppingTrips;

public class ShoppingTripsRowMapper implements RowMapper<ShoppingTrips> {

	@Override
	public ShoppingTrips mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ShoppingTrips(rs.getInt("retailer_id"), rs.getString("title"),
				new DateTime(rs.getTimestamp("added")));
	}
}
