package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.Banner;

public class BannerRowMapper implements RowMapper<Banner> {
	
	@Override
	public Banner mapRow(ResultSet rs, int rowNum) throws SQLException {
		return Banner.withBrandId(rs.getString("title"),rs.getString("description"),
				rs.getString("image"));
	}
}
