package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.DeviceInfo;
import com.cashback.cashbackarab.models.DeviceType;

public class DeviceInfoRowMapper implements RowMapper<DeviceInfo> {

	@Override
	public DeviceInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new DeviceInfo(rs.getString("push_token"), DeviceType.getDeviceType(rs.getString("app_type")));
	}

}
