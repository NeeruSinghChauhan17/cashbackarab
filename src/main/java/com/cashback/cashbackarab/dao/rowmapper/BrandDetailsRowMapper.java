package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.brand.Brand;

public class BrandDetailsRowMapper implements RowMapper<Brand> {

	@Override
	public Brand mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Brand(rs.getInt("retailer_id"), rs.getString("title"), rs.getString("image"), null,
				rs.getInt("deal_of_week"), rs.getString("cashback"), rs.getString("description"), rs.getString("url"),
				null, null, null, (rs.getInt("favorite_id") > 0 ? true : false), null,rs.getString("conditions"),
				rs.getTimestamp(("added")), rs.getString("banner"), null);
	}

}
