package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.Email;

public class EmailRowMapper implements RowMapper<Email> {

	@Override
	public Email mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Email(rs.getString("language"), rs.getString("email_name"), rs.getString("email_subject"),
				rs.getString("email_message"));
	}
}
