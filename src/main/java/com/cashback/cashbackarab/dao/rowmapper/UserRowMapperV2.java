package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.InternationalNumber;
import com.cashback.cashbackarab.models.User;

public class UserRowMapperV2 implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new User(rs.getInt("user_id"), rs.getString("fname"), rs.getString("lname"), rs.getString("email"),
				new InternationalNumber(rs.getString("cc"),rs.getString("area_code"),
				rs.getString("phone_number")),null,
				rs.getString("password"), null,rs.getString("status")=="active"?0:1,rs.getInt("phone_number_verified"),
						rs.getInt("email_verified"),null,
				rs.getString("status"), null, rs.getString("profile_pic"), rs.getInt("notification_status"));
	}

}