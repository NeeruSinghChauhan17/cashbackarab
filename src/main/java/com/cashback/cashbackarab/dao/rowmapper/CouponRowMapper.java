package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.brand.Coupon;

public class CouponRowMapper implements RowMapper<Coupon> {

	@Override
	public Coupon mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Coupon(rs.getInt("coupon_id"), rs.getString("link"), rs.getString("code"), rs.getString("title"),
				rs.getString("description"),
				(rs.getString("start_date").contains("0000-00-00") ? null
						: new DateTime(rs.getTimestamp("start_date"))),
				(rs.getString("end_date").contains("0000-00-00") ? null : new DateTime(rs.getTimestamp("end_date"))),
				rs.getInt("visits"));
	}
}