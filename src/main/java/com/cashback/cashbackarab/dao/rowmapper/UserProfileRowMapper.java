package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.Profile;

public class UserProfileRowMapper implements RowMapper<Profile> {

	@Override
	public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		return new Profile(rs.getInt("user_id"), rs.getString("fname"), rs.getString("lname"), 
				(rs.getString("email").equals(null) || rs.getString("email").equals("") )?rs.getString("optional_email"):rs.getString("email"),
				rs.getString("password"), rs.getString("phone"), rs.getString("address"), rs.getString("state"),
				rs.getString("city"), rs.getInt("country"), rs.getString("zip"), null, rs.getString("profile_pic"));
	}

}
