package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.Token;

public class TokenRowMapper implements RowMapper<Token> {

	@Override
	public Token mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Token(rs.getInt("user_id"), rs.getString("unique_id"),
				new DateTime(rs.getTimestamp("creation_date_time")));
	}

}