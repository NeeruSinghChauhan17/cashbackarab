package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
public class SettingRowMapper implements RowMapper<Map>{

	@Override
	public Map<String,Integer> mapRow(ResultSet rs, int arg1) throws SQLException {
		Map<String,Integer> map=new HashMap<String,Integer>();
		map.put(rs.getString("setting_key"),rs.getInt("setting_value"));
		return map;
	}

}
