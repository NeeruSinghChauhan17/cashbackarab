package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.Review;

public class BrandReviewRowMapper  implements RowMapper<Review> {

	@Override
	public Review mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		return new Review(null,rs.getString("review_title"),rs.getString("review"),
				rs.getFloat("rating"),null,rs.getString("username"),rs.getString("fname"),rs.getString("lname"),
				new DateTime(rs.getTimestamp("added")));
		
	}

}
