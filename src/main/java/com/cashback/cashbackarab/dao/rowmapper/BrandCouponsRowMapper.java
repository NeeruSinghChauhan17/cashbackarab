package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.models.brand.Coupon;

public class BrandCouponsRowMapper implements RowMapper<Brand> {

	@Override
	public Brand mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Brand(rs.getInt("retailer_id"), rs.getString("title"), rs.getString("image"), null, null,
				rs.getString("cashback"), null, rs.getString("url"), null, null, null, null, null, null, null, null,
				new Coupon(rs.getInt("coupon_id"), rs.getString("coupon_link"), rs.getString("coupon_code"),
						rs.getString("coupon_title"), rs.getString("coupon_description"),
						(rs.getString("start_date").contains("0000-00-00") ? null
								: new DateTime(rs.getTimestamp("start_date"))),
						(rs.getString("end_date").contains("0000-00-00") ? null
								: new DateTime(rs.getTimestamp("end_date"))),
						rs.getInt("visits")));
	}
}
