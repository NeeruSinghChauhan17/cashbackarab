package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.Profile;

public class UserDealsActivationRowMapper implements RowMapper<Profile> {

	@Override
	public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Profile(rs.getInt("user_id"), null, null, null, null, null, null, null, null, null, null, null,
				null);
	}

}
