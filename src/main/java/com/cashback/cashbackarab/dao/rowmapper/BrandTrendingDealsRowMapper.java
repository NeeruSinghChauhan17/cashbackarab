package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.brand.Brand;
import com.cashback.cashbackarab.models.brand.Category;

public class BrandTrendingDealsRowMapper implements RowMapper<Brand> {

	@Override
	public Brand mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Brand(rs.getInt("retailer_id"), rs.getString("title"), rs.getString("image"), null, null,
				rs.getString("cashback"), null, rs.getString("url"), null, null,
				(rs.getString("end_date").contains("0000-00-00") ? null : new DateTime(rs.getTimestamp("end_date"))),
				null,rs.getInt("featured")>0? true:false, null, null, null, null);
	}

}
