package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.User;

public class UserExistanceRowMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new User(rs.getInt("user_id"), rs.getString("fname"), rs.getString("lname"), rs.getString("email"),null, null,
				rs.getString("password"), null, null,null, null, null, null, null, null,null);
	}

}
