package com.cashback.cashbackarab.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cashback.cashbackarab.models.brand.Brand;

public class BrandTrendingOfferRowMapper implements RowMapper<Brand> {
	@Override
	public Brand mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Brand(0, rs.getString("title"), rs.getString("image"), null, 0, rs.getString("cashback"), null, null,
				null, null, null, false, null,null, null, null, null);
	}

}
