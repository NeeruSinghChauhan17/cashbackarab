package com.cashback.cashbackarab.dao;

import com.cashback.cashbackarab.models.Email;

public interface MailDao {
	
	Email getEmailContent(String emailName);
}
