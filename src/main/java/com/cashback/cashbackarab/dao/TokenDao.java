package com.cashback.cashbackarab.dao;

import java.util.Collection;

import com.cashback.cashbackarab.models.Token;
import com.google.common.base.Optional;

public interface TokenDao {

	void saveUserToken(final Token token, final String appType,final  String deviceId);

	void updateUserToken(final Token token, final String appType,final String deviceId);

	Token getUserToken(final Integer userId, final String appType,final String deviceId);

	Optional<Token> getUserSession(final Token token,final  String appType,final String deviceId);

	public Collection<Token> isUserTokenExistWithApp(final Integer userId,final String appType);

	void deleteAllTokenByUserId(final Integer userId);

	void invalidateToken(final Integer userId,final  String appType,final  String deviceId);
}
