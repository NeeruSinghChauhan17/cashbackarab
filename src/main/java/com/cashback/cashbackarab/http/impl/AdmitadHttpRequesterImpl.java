package com.cashback.cashbackarab.http.impl;

import java.util.Arrays;
import java.util.Optional;
import javax.xml.ws.http.HTTPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.cashback.cashbackarab.http.AdmitadHttpRequester;
import com.cashback.cashbackarab.models.http.AdmitadToken;
import com.cashback.cashbackarab.models.http.TrackingReport;
import com.cashback.cashbackarab.service.TransactionService;


@Component
public class AdmitadHttpRequesterImpl implements AdmitadHttpRequester {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdmitadHttpRequesterImpl.class);
    private final static String ADMITAD_ROOT_URL = "https://api.admitad.com/";
	private final static String GRANT_TYPE = "client_credentials";
	private final static String CLIENT_ID = "787cd0cb29234b589e58269e82596e";
	private final static String SCOPE = "statistics";
	private final static String ADMITAD_TOKEN_AUTHORIZATION = "Basic Nzg3Y2QwY2IyOTIzNGI1ODllNTgyNjllODI1OTZlOjQxMjgwNmZjMzJhMmQ5M2EwZTZlMWU0MWViYTc1NQ==";
	
	
	@Autowired
	private TransactionService transactionService;

	public Optional<String> getToken() {
		StringBuilder builder = new StringBuilder();
		builder.append(ADMITAD_ROOT_URL);
		builder.append("token/");
		builder.append("?grant_type=");
		builder.append(AdmitadHttpRequesterImpl.GRANT_TYPE);
		builder.append("&client_id=");
		builder.append(AdmitadHttpRequesterImpl.CLIENT_ID);
		builder.append("&scope=");
		builder.append(AdmitadHttpRequesterImpl.SCOPE);
		String SERVER_URL = builder.toString();
		final HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set("Authorization", ADMITAD_TOKEN_AUTHORIZATION);
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AdmitadToken> response = restTemplate.exchange(SERVER_URL, HttpMethod.POST, entity,AdmitadToken.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			return Optional.ofNullable(response.getBody().getAccessToken());
		}
		LOGGER.error("==============Error while getting Access Token===============");
		return Optional.empty();

	}

	public void getTrackingReport(final String token,final String startDate,final String endDate, Integer offset,final Integer limit) {
		StringBuilder builder = new StringBuilder();
		builder.append(ADMITAD_ROOT_URL);
		builder.append("/statistics/actions/");
		builder.append("?date_start=");
		builder.append(startDate);
		builder.append("&date_end=");
		builder.append(endDate);
		builder.append("&limit=" + limit);
		builder.append("&offset=" + offset);
		builder.append("&order_by=clicks");

		String SERVER_URL = builder.toString();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set("Authorization", "Bearer" + " " + token);
        try{
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<TrackingReport> response = restTemplate.exchange(SERVER_URL, HttpMethod.GET, entity,
				TrackingReport.class);
		
		if (response.getStatusCode() == HttpStatus.OK) {
			 transactionService.giveCashback(response.getBody().getResults());
			if (response.getBody().getResults().size() == limit) {
				  getTrackingReport(token,startDate, endDate, offset + limit, limit);
			}
          }
      }
	 catch(HTTPException e){
      LOGGER.error("=================Error while getting Tracking Report===========",e );	
	
   }
 }

}
