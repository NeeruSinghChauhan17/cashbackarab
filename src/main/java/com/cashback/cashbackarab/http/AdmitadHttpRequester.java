package com.cashback.cashbackarab.http;

import java.util.Optional;

import com.cashback.cashbackarab.models.http.TrackingReport;


public interface AdmitadHttpRequester {

	Optional<String> getToken();

	void getTrackingReport(String token,String startDate, String endDate, Integer offset, Integer limit);

}
