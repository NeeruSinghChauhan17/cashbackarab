package com.cashback.cashbackarab.provider.filter;

import java.io.IOException;
import java.util.Set;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cashback.cashbackarab.annotation.Secured;
import com.cashback.cashbackarab.service.TokenService;
import com.google.common.collect.Sets;


@Secured
@Service
public class AuthorizationFilter implements ContainerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationFilter.class);

	private final TokenService tokenService;

	@Autowired
	public AuthorizationFilter(TokenService tokenService) {
		this.tokenService = tokenService;
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		 final Set<String> OPEN_URIS = Sets.newHashSet("cashback/api/v1/brand/trendingstores", "cashback/api/v1/brand/getalltrendingstores","cashback/api/v1/brand/branddetails",
				"cashback/api/v1/brand/samecategory");

		final String deviceId = requestContext.getHeaderString("User-Agent");
		final String appType = requestContext.getHeaderString("Device-Type");
		final String token = requestContext.getHeaderString("Token");
		final MultivaluedMap<String, String> pathParameters = requestContext.getUriInfo().getPathParameters();
		 String userId = pathParameters.getFirst("user_id");
		 
		if (OPEN_URIS.contains(requestContext.getUriInfo().getPath()) ||(requestContext.getUriInfo().getPath().contains("/branddetails")) 
				|| (requestContext.getUriInfo().getPath().contains("/samecategory")) ) {
			final MultivaluedMap<String, String> query = requestContext.getUriInfo().getQueryParameters();
			 userId = query.getFirst("user_id");
			 
			if ((userId == null  && token==null) || (userId==null && token!=null)) {
				return;
			}
		}
		if (token == null) {
			LOGGER.info("Aborting unauthorized request for userId={} and token={}", userId, token);
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity("Token Not Present").build());
			return;
		}
		if (!tokenService.verifyUserToken(Integer.valueOf(userId), token, appType, deviceId)) {
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity("Token Not Valid").build());
		}
	}

}
