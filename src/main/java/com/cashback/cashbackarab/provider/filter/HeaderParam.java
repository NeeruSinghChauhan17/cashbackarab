package com.cashback.cashbackarab.provider.filter;

public interface HeaderParam {

	public static final String USER_AGENT = "User-Agent";
	public static final String AUTHORIZATION = "Authorization";
	public static final String DEVICE_TOKEN = "Device-Token";
	public static final String DEVICE_TYPE = "Device-Type";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String TOKEN = "Token";
	
	
	public static final String SCOPE = "scope";

}
