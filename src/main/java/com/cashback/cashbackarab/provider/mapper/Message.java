package com.cashback.cashbackarab.provider.mapper;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {

	private final String cause;
	
	@SuppressWarnings("unused")
	private Message() {
		this(null);
	}

	public Message(final String cause) {
		this.cause = cause;
	}

	@JsonProperty("cause")
	public String getCause() {
		return cause;
	}
	
	
	
}
