package com.cashback.cashbackarab.jobs;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cashback.cashbackarab.http.impl.AdmitadHttpRequesterImpl;
import com.cashback.cashbackarab.utils.DateUtil;


@Component
public class Cron {


	private final Integer OFFSET= 0;
	private final Integer LIMIT=20; 
	private static final Logger LOGGER = LoggerFactory.getLogger(Cron.class);
	
	
	@Autowired
	AdmitadHttpRequesterImpl admitadHttpRequester;
	
 
	
    @Transactional
	@Scheduled(cron="1 1 0 * * *?")              //uncomment
    public void scheduleJobs() {
    final String startDate="23.08.2017";    //appended userId in url as subId4 on 23.08.2017   and on 11.01.108 discount in subId3 
		String yesterdayDate = DateUtil.getYesterdayDateString();
	
		Optional<String> accessToken = admitadHttpRequester.getToken();
		try{
		      if(accessToken.isPresent()){
		    	   admitadHttpRequester.getTrackingReport(accessToken.get(),startDate, yesterdayDate,OFFSET,LIMIT);       
		         //admitadHttpRequester.getTrackingReport(accessToken.get(),"10.01.2018", "07.02.2018",OFFSET,LIMIT);
		          return;
		         }
		    }
		    catch(Exception e){
		   LOGGER.error("=================Error while Automated Tracking==============",e); 
         }
	}
}
